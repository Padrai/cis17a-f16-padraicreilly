#include <iostream>
#include <ctime>
using namespace std;

//FUNCTIONS FOR PART 1
void fillArr(int * p, int range, int size);
int * makeArr(int size);
void printArr(int * p, int size);

int main() 
{
	srand(time(0));
	
	//PArt 1(MAIN) -----------------------------------------------------------------------
	
	cout<<"PART 1"<<endl;
	cout<<"What range of random numbers?"<<endl;
	
	int range;
	cin>>range;
	
	cout<<"How many values?"<<endl;
	int size;
	cin>>size;
int * arr = makeArr(size);

	fillArr(arr, range, size); 
	printArr(arr, size);
	cout<<endl;
	

	return 0;
}

int * makeArr(int size)
{
	int * p = new int[size];
	return p;
}

void fillArr(int * p, int range, int size) { for (int i = 0; i < size; i++) { p[i] = rand() % range + 1; } }

void printArr(int * p, int size)
{
	for (int i = 0; i < size; i++) { 
		cout << p[i] << " ";
		if (i % 5 == 0 && i!=0) { cout << endl; }
	}
}