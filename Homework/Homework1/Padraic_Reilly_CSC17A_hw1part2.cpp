#include <iostream>
using namespace std;

string * deleteEntry(string * arr, int& size, int loc);

int main()
{
    int size = 5;
    int loc;
    string * nameList = new string[size];
    nameList[0] = "First";
    nameList[1] = "Second";
    nameList[2] = "Third";
    nameList[3] = "Fourth";
    nameList[4] = "Fifth";
    cout<<"List is:"<<endl;
    for(int i = 0; i<size; i++){cout<<i+1<<". "<< nameList[i]<<endl;}
    cout<<endl<<"Which entry would you like to remove?"<<endl;
    cin>>loc;
    cout<<"Removing line "<<loc<<":"<<endl<<endl;
    
    nameList= deleteEntry(nameList, size, loc);
    for(int i = 0; i<size; i++){cout<<i+1<<". "<<nameList[i]<<endl;}
    
    return 0;
}

string * deleteEntry(string * arr, int &size, int loc)
{
    if (loc>size||loc<=0){cout<<"invalid location accessed. program failure."<<endl; exit(0);}
    else {size--;}
    
    string * temp = new string[size];
    for(int i = 0; i<loc-1; i++){temp[i]=arr[i];}
    for(int i = loc; i<size+1; i++){temp[i-1]=arr[i];}
    
    return temp;
  
    
    
}
