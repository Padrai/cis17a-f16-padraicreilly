#include <iostream>
#include <ctime>

using namespace std;

int coinToss();

int main()
{   srand(time(0));
    int numFlips;
    
    do{cout<<"Enter number of flips: "<<endl;
    cin>>numFlips;}
    while(numFlips<1);
    
    for(int i=0; i<numFlips;i++){cout<<i + 1<<". "; coinToss(); cout<<endl; }
    
    return 0;
}

int coinToss()
{int toss = rand()%2 + 1;
    if(toss == 1){cout<<"Heads"<<endl; return 1;}
    else {cout<<"Tails"<<endl; return 2;}
}