#include <iostream>
using namespace std;

double avgScores(int * list, int size);
void bubbleSort(int * list, int size);
void printScores(int * list, int size);

int main()
{   int size; 
    cout<<"How many scores do you have to enter?"<<endl;
    cin>>size;
    int * scores = new int[size];
    cout<<"Ok, begin entering them"<<endl;
    
    for (int i = 0; i< size; i++){do{cin>>scores[i];}while(scores[i]<0);}
    cout<<endl<<"The average is: "<< avgScores(scores, size)<<endl;
    bubbleSort(scores, size);
    
    printScores(scores, size);
    
    return 0;   
}

double avgScores(int * list, int size)
{   int sum = 0; 
for (int i = 0; i < size; i++){sum += list[i];}
double avg = static_cast<double>(sum) / static_cast<double>(size);

    return avg;
}



void bubbleSort(int * list, int size)
{cout<<"Bubble-swapping your scores to ascending order"<<endl;
    int temp;
    bool swap;
    do
    {
        swap = false;
        for(int i = 0; i <size - 1; i++)
            {
                if (list[i]> list[i+1])
                    {
                        temp = list[i];
                        list[i] = list[i + 1];
                        list[i+1] = temp;
                        swap = true;
                    }
            }
    }while(swap);
}

void printScores(int * list, int size)
{   cout<<endl<<"Printing Scores"<<endl;
    for (int i = 0; i< size; i++){cout<<list[i]<<endl;}
    
}