#include <iostream>
using namespace std;

double avgScores(int * list, int size);
int * bubbleSort_n_Trim(int * list, int &size);
void printScores(int * list, int size);


int main()
{   int size; 
    cout<<"How many scores do you have to enter?"<<endl;
    cin>>size;
    int * scores = new int[size];
    cout<<"Ok, begin entering them"<<endl;
    
    for (int i = 0; i< size; i++){do{cin>>scores[i];}while(scores[i]<0);}
    
   scores = bubbleSort_n_Trim(scores, size);
    
   
    
    printScores(scores, size);
    
    cout<<endl<<"The average is: "<< avgScores(scores, size)<<endl;
    
    return 0;   
}

double avgScores(int * list, int size)
{   int sum = 0; 
for (int i = 0; i < size; i++){sum += list[i];}
double avg = static_cast<double>(sum) / static_cast<double>(size);


    return avg;
}



int * bubbleSort_n_Trim(int * list, int& size)
{cout<<endl<<"Bubble-swapping your scores to ascending order and trimming lowest score."<<endl;
    int bubtemp;
    bool swap;
    do
    {
        swap = false;
        for(int i = 0; i <size - 1; i++)
            {
                if (list[i]> list[i+1])
                    {
                        bubtemp = list[i];
                        list[i] = list[i + 1];
                        list[i+1] = bubtemp;
                        swap = true;
                    }
            }
    }while(swap);
    int * temp = new int[--size];
    for(int i =0; i<size;i++)
    {
        temp[i] = list[i + 1];
    }
return temp;

    
}

void printScores(int * list, int size)
{   cout<<endl<<"Printing scores:"<<endl;
    for (int i = 0; i< size; i++){cout<<list[i]<<endl;}
    
}

