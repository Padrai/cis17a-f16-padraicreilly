#include <iostream>
#include <fstream>
#include <string>

using namespace std;



double avgScores(int * list, int size);
int * addItem(int * list, int &size, int newValue);
int * bubbleSort_n_Trim(int * list, int &size);
void printScores(int * list, int size);


int main()

{   int size = 0;
int * scores = new int[size];

    ifstream iFile("text.txt");
    if (iFile.is_open())
    {   string input;
        while(getline(iFile, input))
        {
            int newValue = atoi(input.c_str());
            scores = addItem(scores, size, newValue);
        }
        iFile.close();
        
    }
    
    
    
    
    scores = bubbleSort_n_Trim(scores, size);
    
   
    
    printScores(scores, size);
    
    cout<<endl<<"The average is: "<< avgScores(scores, size)<<endl;
    
    return 0;   
}

double avgScores(int * list, int size)
{   int sum = 0; 
for (int i = 0; i < size; i++){sum += list[i];}
double avg = static_cast<double>(sum) / static_cast<double>(size);


    return avg;
}



int * bubbleSort_n_Trim(int * list, int& size)
{cout<<endl<<"Bubble-swapping your scores to ascending order and trimming lowest score."<<endl;
    int bubtemp;
    bool swap;
    do
    {
        swap = false;
        for(int i = 0; i <size - 1; i++)
            {
                if (list[i]> list[i+1])
                    {
                        bubtemp = list[i];
                        list[i] = list[i + 1];
                        list[i+1] = bubtemp;
                        swap = true;
                    }
            }
    }while(swap);
    int * temp = new int[--size];
    for(int i =0; i<size;i++)
    {
        temp[i] = list[i + 1];
    }
return temp;

    
}

void printScores(int * list, int size)
{   cout<<endl<<"Printing scores:"<<endl;
    for (int i = 0; i< size; i++){cout<<list[i]<<endl;}
    
}

int * addItem(int * list, int &size, int newValue)
{
    int * ret = new int[size + 1];
    for (int i = 0; i< size; i++){ret[i]= list[i];}
    ret[size] = newValue;
    size++;
    return ret;
    
}