/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 2
* Problem:1
* I certify this is my own work and code
*/
#include <iostream>
#include <ctime>

int * arrReverse(int * arr, int size);
int main()
{   //seed RNG and create test value limitations
    srand(time(0));
    int size = rand()%10 + 2;
    int range = rand()%50 + 1;
    //create fill and display array
    std::cout<<"Here is your array of random size "<<size<< " and of random range "<<range<<std::endl;
    int * anyArray = new int[size];
    for(int i = 0;  i <size;  i++){anyArray[i] = rand()%range + 1;}
    for(int i = 0;  i <size;  i++){std::cout<<anyArray[i]<<std::endl<<std::endl;}
    //call reversing function
    anyArray = arrReverse(anyArray, size);
    //print again to display results
    std::cout<<"Your new reversed array is: "<<std::endl;
    for(int i = 0;  i <size;  i++){std::cout<<anyArray[i]<<std::endl<<std::endl;}
    return 0;
    
    
}
int * arrReverse(int * arr, int size)
{
    int * flippedArr = new int [size];
    for(int i = 0; i< size; i++){flippedArr[i] = arr[size- i - 1];}
    return flippedArr;
    
}