/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 2
* Problem:2
* I certify this is my own work and code
*/
#include <iostream>

double calcAvg(int * arr2bsummed, int size);
double findMedian(int * array, int size);
int findMode(int * arr2Mode, int size);

int main()
{   std::cout<<"How many students were surveyed? "<<std::endl;
    int size;
    do{std::cin>>size;} while(size < 0);
    int * surveyResults = new int[size];
    std::cout<<"Enter their results."<<std::endl;
    for (int i = 0; i< size; i++)
    {
        do{std::cin>>surveyResults[i];}while(surveyResults[i] < 0);
    }
    std::cout<<"All results recieved."<<std::endl;
    
    std::cout<<"Average is: "<<calcAvg(surveyResults,size)<<std::endl<<std::endl;
    std::cout<<"The median is "<<findMedian(surveyResults, size)<<std::endl<<std::endl;
    std::cout<<"The mode is "<< findMode(surveyResults, size)<<std::endl<<std::endl;
    
    return 0;
}

double findMedian(int * array, int size)
{
    
    //sort
    bool swap;
    do
    {
        swap = false;
        for(int i = 0; i<size-1;i++)
        {
            if(array[i]<array[i + 1])
            {
                swap = true;
                int temp = array[i];
                array[i] = array[i + 1];
                array[i+1] = temp;
            }
        }
    }
    while(swap);
    std::cout<<std::endl<<std::endl<<"Here is sorted array"<<std::endl;
    for(int i = 0; i <size; i++){std::cout<<array[i]<<std::endl;}
    std::cout<<std::endl;
    //find median
     double median;

    if (size%2 ==0)
    { 
        median = static_cast<double>(((array[size/2 - 1] + array[(size/2) ])))/2.0; 
        return median;
    }
    
    else
    {
        median = static_cast<double>(array[size/2]);
        return median;
        
    }  

    
}





double calcAvg(int * arr2bsummed, int size)
{   int sum= 0;
    double avg;
    for (int i = 0; i< size; i++)
    {  
        sum += arr2bsummed[i];
    }
    avg = static_cast<double>(sum)/static_cast<double>(size);
    return avg;
}

int findMode(int * arr2Mode, int size)
{   bool swap;
    do
    {
        swap = false;
        for(int i = 0; i<size-1;i++)
        {
            if(arr2Mode[i]<arr2Mode[i + 1])
            {
                swap = true;
                int temp = arr2Mode[i];
                arr2Mode[i] = arr2Mode[i + 1];
                arr2Mode[i+1] = temp;
            }
        }
    }
    while(swap);
    //array must be sorted 
    int mode;
    int maxVal = arr2Mode[0];
    
    //declare new array
   
    int * freqCount = new int[maxVal];
    //fill array w zeros
    for (int i = 0; i< maxVal; i++){freqCount[i]=0;}
    //count occurences
    for (int i = 0; i< maxVal; i++)
    { 
        for (int j = 0; j < size; j++){if(arr2Mode[j] == i){freqCount[i]++;}}
    }
    //find location of largest value
    int currFreqMax = 0;
    int currFreqMaxLoc = -1; 
    for (int i = 0; i< maxVal; i++)
    {
        if (freqCount[i]>currFreqMax)
        {   
            currFreqMax=freqCount[i];
            currFreqMaxLoc=i;
            
        }
        
    }
    //return location + 1;
    return currFreqMaxLoc
    ;
    
    
    
}