/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 2
* Problem:3
* I certify this is my own work and code
*/
#include <iostream>
int stringLength(char * phrase);
int main()
{   
    char * word = new char[1];
    std::cout<<"Enter a word or phrase to find the length of:"<<std::endl;
    std::cin.getline(word,1000);
    std::cout<<"length is "<<stringLength(word);
    
    return 0;
    
}

int stringLength(char * phrase)
{
    int i = 0;
    while(phrase[i] != '\0'){i++;}
    return i;
}