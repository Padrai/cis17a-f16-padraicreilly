/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 2
* Problem:5
* I certify this is my own work and code
*/
#include <iostream>

int stringLength(char * phrase);
int main()
{
    char * firstName = new char[1];
    char * middleName = new char[1];
    char * lastName = new char[1];
    int fSize, mSize, lSize;
    std::cout<<"Enter your first name:"<<std::endl;
    std::cin.getline(firstName,100);
    fSize = stringLength(firstName);
    
    std::cout<<"Enter your middle name:"<<std::endl;
    std::cin.getline(middleName,100);
    mSize = stringLength(middleName);
    
    std::cout<<"Enter your last name:"<<std::endl;
    std::cin.getline(lastName,100);
    lSize = stringLength(lastName);
    //make new array for full name
    char * fullName = new char[fSize + mSize + lSize + 3];
    //add last name to beginning
    for (int i = 0; i< lSize; i++){fullName[i] = lastName[i];}
    fullName[lSize] = ',';
    fullName[lSize + 1] = ' ';
    //add middle name
    for (int i = lSize + 2; i < fSize + 2 + fSize; i++){fullName[i] = firstName[i - 2 - lSize];}
    fullName[lSize + fSize + 2] = ' ';
    for (int i = lSize + fSize + 3; i< lSize + fSize + 3 + mSize; i++)
    {
        fullName[i] = middleName[i - lSize - 3 - fSize];
    }
    int fullSize = fSize + mSize + lSize + 3;
    
    
    std::cout<<"Here is the full name, appropriately formatted;"<<std::endl;
    for (int i = 0; i<fullSize; i++){std::cout<<fullName[i];}
    
    return 0;
}
int stringLength(char * phrase)
{
    int i = 0;
    while(phrase[i] != '\0'){i++;}
    return i;
}