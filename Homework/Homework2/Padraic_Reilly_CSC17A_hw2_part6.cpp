/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 2
* Problem:6
* I certify this is my own work and code
*/
#include <iostream>
#include <cstdlib>

int sumDigits(char * list, int size);

int main()
{  
    int size;
    std::cout<<"How many single digits do you want to sum?"<<std::endl;
    std::cin>>size;
    std::cout<<"Ok, enter them now:"<<std::endl;
    char * list = new char[size];
    std::cin>>list;
    for(int i = 0; i<size; i++){std::cout<<list[i]<<std::endl;}
    std::cout<<"sum of digits above is "<<sumDigits(list, size)<<std::endl;
    return 0;
}




int sumDigits(char * list, int size)
{
    int sum = 0;
    for (int i = 0; i< size; i++)
    {   
      int temp = list[i] - 48;
        sum += temp;
    }
    return sum;
}