/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 10/1 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 5
* Problem:1
* I certify this is my own work and code
*/
#include <iostream>
using namespace std;

void printList(double *);

class numArray
{
    public: 
        double * numList ;
        int size;
        numArray(int);
        ~numArray();
        int returnSize();
        double * retArrayAddress();
        void fillSpot(int); 
        double returnSpotContents(int);
        double returnHighestValue(double* , int);
        double returnLowestValue(double * , int);
        double calcAndReturnAvg(double * , int);
};

numArray::numArray(int sz)
{
    numList = new double[sz];
    size = sz;
}

numArray::~numArray(){//;} 
    cout<<"Deconstructor is starting"<<endl;
    delete[] numList;
    cout<<"Deconstructor finished"<<endl;
}


void numArray::fillSpot(int spot)
{
    cout<<"Enter your floating point for number for spot #"<<spot + 1<<endl; 
    double temp;
    cin>>temp;
    numList[spot] = temp;
    
    cout<<"Recieved!"<<endl;
}

double* numArray::retArrayAddress(){return numList;}

double numArray::returnSpotContents(int spot)
{
    return numList[--spot];
}

double numArray::returnHighestValue(double* numList1, int size)
{
    int count = 0;
    bool swap;
    do 
    {
        swap = false;
        for (int i = 0; i< size - 1; i++)
        {
            if( numList1[i] < numList1[i + 1] )
            {
                swap = true;
                double temp = numList1[i];
                numList1[i] = numList1[i + 1];
                numList1[i + 1] = temp;
                
            }
        }
        
    }
    while(swap);
    
    return numList1[0];
    
    
}


double numArray:: returnLowestValue(double * numList1, int size)
{
    int count = 0;
    bool swap;
    do 
    {
        swap = false;
        for (int i = 0; i < size - 1; i++)
        {
            if( numList1[i] > numList1[i + 1] )
            {
                swap = true;
                double temp = numList1[i];
                numList1[i] = numList1[i + 1];
                numList1[i + 1] = temp;
            }
        }
        
    }
    while(swap);
    return numList1[0];
    
}

double numArray:: calcAndReturnAvg(double * numList1, int size)
{
    double avg = 0;
    int sum = 0;
    for (int i = 0; i <size; i ++){sum += numList[i];}
    avg = (sum / static_cast<double>(size));
    return avg;
}

void printList(double * list, int size)
{
    cout<<endl<<"Your current array:"<<endl;
    for (int i = 0; i< size; i++){cout << i + 1<<". "<<list[i]<<endl;}
    cout<<endl<<endl;
}
int main()
{   
    int tempSize;
    //create array of positive size
    do {
        cout<<"Enter the size of the array you want."<<endl; 
        cin>>tempSize;
       } while(tempSize <= 0);
    numArray myArray(tempSize);
    //fill array with values
    cout<<"Enter your values"<<endl;
    for (int i = 0; i < tempSize; i++)
    {
        myArray.fillSpot(i);
    
        
    }
    
    printList(myArray.numList, tempSize);
    
    
    cout<<"The highest Value: "<<myArray.returnHighestValue(myArray.numList, tempSize)<<endl;
    cout<<"The lowest value:"<< myArray.returnLowestValue(myArray.numList, tempSize )<<endl;
    cout<<"The average is: "<<myArray.calcAndReturnAvg(myArray.numList, tempSize)<<endl;
    cout<<"Array is now sorted in increasing order."<<endl<<endl;
    
    bool quit = false;
    while (!quit)
    {   
        printList(myArray.numList, tempSize);
        cout<<"would you like to access an element(a), change an element(c), view the elements again(v), or quit(q)?";
        char userInput;
        int userNumInput;
        cin>> userInput;
        if(userInput == 'a')
        {
           
            do
            {   
                cout<<"Which element would you like to access?"<<endl;
                cin>>userNumInput;
            }while(userNumInput > tempSize || userNumInput <= 0);
            
            cout<<"Element number "<<userNumInput<<": "<< myArray.numList[userNumInput - 1]<<endl;
            
        }
        
       else if(userInput == 'c')
        {
            do
            {    
                cout<<"Which element would you like to access?"<<endl;
                cin>>userNumInput;
            }
            while(userNumInput > tempSize || userNumInput < 1);
            double userDubInput; 
            cout<<"Okay, enter your new value for entry #"<<userNumInput<<endl;
            cin>>userDubInput;
            myArray.numList[userNumInput - 1] = userDubInput;
        }
        
        else if(userInput == 'v'){cout<<"Okay, showing array again:"<<endl; }
       
       else if(userInput == 'q')
        {
           quit = true; 
        }
        
    }
    
    
    return 0;
       
       
       
}