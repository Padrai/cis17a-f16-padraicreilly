/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 10/1 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 5
* Problem:2
* I certify this is my own work and code
*/
#include <string>
#include<ctime>
#include <iostream>
using namespace std;

class Coin
{
    private:
        string sideUp = " ";
    public:
        Coin();
        void toss();
        string getSideUp();
        
};

Coin::Coin()
{
    int tempRand= rand() % 2;
    if(tempRand == 0){sideUp = "Heads";}
    else{sideUp = "Tails";}
}

void Coin::toss()
{
    int tempRand= rand() % 2;
    if(tempRand == 0){sideUp = "Heads";}
    else if(tempRand == 1){sideUp = "Tails";}   
}
string Coin::getSideUp(){return sideUp;}

int main()
{
    srand(time(0));
    int headsCount = 0;
    int tailsCount = 0;
    int flipsCount = 0;
    int flipsDesired;
    Coin quarter;
    
    cout<<"The quarter has started on "<<quarter.getSideUp()<<"."<<endl;
    cout<<"Enter the desired number of flips of this coin:"<<endl;
    cin>>flipsDesired;
    
    for (int i=0; i< flipsDesired; i++)
    {
        quarter.toss();
        cout<<"Roll # "<<i + 1<<": " <<quarter.getSideUp()<<endl;
        flipsCount++;
        if (quarter.getSideUp() == "Heads")
        {
            headsCount++;
            
                    }
        else if(quarter.getSideUp() == "Tails")
        {
            tailsCount++;
            
        }
    }
    cout<<endl;
    cout<<"Heads: "<<headsCount<<"    Percentage:"<<(static_cast<double>(headsCount)/static_cast<double>(flipsCount))*100<<endl;
    cout<<"Tails: "<<tailsCount<<"    Percentage:"<<(static_cast<double>(tailsCount)/static_cast<double>(flipsCount))*100<<endl;
    cout<<"Rolls: "<<flipsCount<<","<<flipsDesired<<","<<headsCount+tailsCount<<"."<<endl;
    
    
    return 0;
}