/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 10/1 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 5
* Problem:3
* I certify this is my own work and code
*/
#include <iostream>

using namespace std;

class ChemSystem
    {
    private:    
        double temp;
    public:
        
        ChemSystem();
        double getTemp();
        void setTemp(double);
        
        bool isEthylBoiling();
        bool isOxygenBoiling();
        bool isWaterBoiling();
        bool isEthylFreezing();
        bool isOxygenFreezing();
        bool isWaterFreezing();
       
};


ChemSystem::ChemSystem(){cout<<"Chemical system created."<<endl;}

double ChemSystem::ChemSystem::getTemp(){return temp;}

void ChemSystem::setTemp(double temp){this -> temp = temp;}

bool ChemSystem::isEthylBoiling(){if (temp >= 172){return true;}else{return false;}}

bool ChemSystem::isEthylFreezing(){if (temp <= -173){return true;}else{return false;}}

bool ChemSystem::isOxygenBoiling(){if (temp >= -306){return true;}else{return false;}}

bool ChemSystem::isOxygenFreezing(){if (temp <= -362){return true;}else{return false;}}
bool ChemSystem::isWaterBoiling(){if (temp >= 212){return true;}else{return false;}}
bool ChemSystem::isWaterFreezing(){if (temp <= 32){return true;}else{return false;}}

int main()
{
    cout<<"Enter a temp: "<<endl;
    double userTemp;
    cin>> userTemp;
    
    ChemSystem airTank1;
    airTank1.setTemp(userTemp);
    
    if (airTank1.isEthylBoiling() == true){cout<<"Ethyl is boiling"<<endl;}
    else if (airTank1.isEthylFreezing()== true){cout<<"Ethyl is freezing"<<endl;}
    if (airTank1.isOxygenBoiling()== true){cout<<"O2 is boiling"<<endl;}
    else if (airTank1.isOxygenFreezing()== true){cout<<"O2 is freezing"<<endl;}
    if (airTank1.isWaterBoiling()== true){cout<<"H2O is boiling"<<endl;}
    else if (airTank1.isWaterFreezing()== true){cout<<"H2O is freezing"<<endl;}
    
    cout<<endl<<"Program over"<<endl;
  
    
    
    
    
    
    return 0;
    
}

/*
Substance    || Freezing Point || Boiling Point
-----------------------------------------------
Ethyl Alcohol |   −173      |    172
Oxygen        |   −362      |   −306
Water         |     32      |    212
*/
