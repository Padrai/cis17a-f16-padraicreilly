/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 10/1 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 5
* Problem:4
* I certify this is my own work and code
*/
#include <string>
#include <iostream>
#include <vector>
using namespace std;
//Inventory Item copied/Pasted from page 763 - Padraic E Reilly


    typedef class InventoryItem
     {  private:
            string description; // The item description
            double cost; // The item cost
            int units; // Number of units on hand
        public:
     // Constructor #1
          InventoryItem()
        { // Initialize description, cost, and units.
            description = "";
            cost = 0.0;
            units = 0;
        }
    
     // Constructor #2
        InventoryItem(string desc)
        { // Assign the value to description.
            description = desc;
     // Initialize costand units.
            cost = 0.0;
            units = 0; 
        }
    
     // Constructor #3
     InventoryItem(string desc, double c, int u)
     { // Assign values to description, cost, and units.
    description = desc;
    cost = c;
    units = u; }
    
     // Mutator functions
     void setDescription(string d)
     { this -> description = d; }
    
     void setCost(double c)
     { cost = c; }
    
     void setUnits(int u)
     { units = u; }
    
     // Accessor functions
     string getDescription() const
     { return description; }
    
     double getCost() const
     { return cost; }
     int getUnits() const
     { return units; }
 } InventoryItem;

typedef class CashRegister
{
    private:
        vector<InventoryItem> storeInventory;    
    public: 
     void addProducts(vector<InventoryItem>&);
     CashRegister();
     int getIndex(string search, vector<InventoryItem> pList);
     void printInventory(std::vector<InventoryItem> pList);
     
} CashRegister;

CashRegister::CashRegister(){cout<<"Register Opened. Begin Transactions for the day!"<<endl;}

void CashRegister::addProducts(vector<InventoryItem>& ProductList)
{
    //how may products to add
    int howManyProducts;
    cout<<"Hello, Store Manager. How many products would you like to add?"<<endl;
    cin>> howManyProducts;
    for (int i = 0; i<howManyProducts; i++)
    {   //get info to create each project
        string tempName;
         cout<<"Enter product Name"<<endl;
        cin>>tempName;
        double tempCost;
        do{ 
            cout<<"Enter Product Cost:"<<endl;
            cin>>tempCost;} while(tempCost <= 0.0);
        
        int tempUnits;
        
        do{ cout<<"Enter Units on Hand: "<<endl;
            cin>>tempUnits;
            }while(tempUnits<=0);
        //create and push_back into ProductList
        InventoryItem item1(tempName, tempCost, tempUnits);
        ProductList.push_back(item1); 
    }
}

int CashRegister::getIndex(string search, vector<InventoryItem> pList)
{   
    int index = -1;
    for (int i = 0; i< pList.size(); i++)
    {
        if (search == pList[i].getDescription() ) {index = i; }
    }

    return index;
}
void CashRegister::printInventory(vector<InventoryItem> pList)
{
    for (int i =0 ; i<pList.size(); i++)
        {   
            cout<<endl<<i + 1<<") DESC:"
            <<pList[i].getDescription()<<"\t COST: "
            <<pList[i].getCost()<<"\t UNITS: "<<pList[i].getUnits()<<endl;
        }
    
}


//Inventory Item copied/Pasted from page 763 - Padraic E Reilly
//void addItemsToProductsList(const InventoryItem&);
int main()
{   
    CashRegister register1;
    vector<InventoryItem>List(0);
    register1.addProducts(List);
    register1.printInventory(List);
     
    int index = -1;
    bool quit = false;
    double cost = 0.0;
    
    do{  
        cout<<"Which product would you like to purchase?"<<endl;
        string searchTerm;
        cin>>searchTerm;
        
        if (register1.getIndex(searchTerm, List) > -1)
        {  
            index = register1.getIndex(searchTerm, List);
            quit = true;
            cost = List[index].getCost();
            cout<<"Location: " <<index + 1<<endl;
            cout<<"Cost: "<<cost<<endl;
        }
        else
        {   cout<<"Your search was unable to be completed. Try again"<<endl;}
    }while(!quit);
        
        
        cout<<"Enter the quantity you want to purchase: "<<endl;
        int userQuantity;
        cin>> userQuantity;
   
        
        
        
        
    
    return 0;
}

