/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 10/1 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* HW: 5
* Problem:5
* I certify this is my own work and code
*/
#include <iostream>
#include <string>
using namespace std;
 class DayOfYear
 {
    private:
        int day;
    public:
        DayOfYear(int);
        void print();
 }; 
 
 DayOfYear::DayOfYear(int day)
 {
     cout <<"New query created. Day: "<< day << endl;
     this -> day = day;
 }
 
 void DayOfYear::print()
 {
     string month;
     int dayOfMonth;
     bool overAYear = true;
     do{
         if (day >= 1 && day <= 31 ){month = "January"; dayOfMonth = day; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 32 && day <= 59 ){month = "February"; dayOfMonth = day - 31; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 60 && day <= 90 ){month = "March"; dayOfMonth = day - 59; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 91 && day <= 120 ){month = "April"; dayOfMonth = day - 90; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 121 && day <= 151 ){month = "May"; dayOfMonth = day - 120; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 152 && day <= 181 ){month = "June"; dayOfMonth = day - 151; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 182 && day <= 212 ){month = "July"; dayOfMonth = day - 181; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 213 && day <= 243 ){month = "August"; dayOfMonth = day - 212; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 244 && day <= 273 ){month = "September"; dayOfMonth = day - 243; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 274 && day <= 304 ){month = "October"; dayOfMonth = day - 273; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 305 && day <= 334 ){month = "November"; dayOfMonth = day - 304; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if (day >= 335 && day <= 365 ){month = "December"; dayOfMonth = day - 334; overAYear = false;
         cout<<month<<" "<<dayOfMonth<<endl;}
         else if(day>365){day -= 365;}
         else {cout<<"Negative number, breaking loop and exiting."<<endl; break; exit(0);}
         
        }while(overAYear);
 }
 
 int main()
 {  int numLoops;
    cout<<"How many days do you want to test?"<<endl;
    cin>> numLoops;
    for (int i = 0; i< numLoops; i++){
     int userEntry;
     cout<<"Enter a day of the year"<<endl;
     cin>>userEntry;
     DayOfYear tester(userEntry);
     tester.print();
     cout<<""<<endl;}
 }
 