#ifndef EMPLOYEETYPES_H
#define EMPLOYEETYPES_H

#include <string>
#include <iostream>
class Employee
{
private:
	std::string name;
	int number;
	std::string hireDate;
public:
	//constructors
	Employee() 
	{	
		name = "";
		number = 0 ;
		hireDate = "0 / 0 / 0";
	}

	//getters
	std::string getName() { return name; }
	int getNumber() { return number; }
	std::string getHireDate() { return hireDate; }
	virtual void printMe() 
	{
		std::cout << "Name:" << name << std::endl;
		std::cout << "Employee Number: " << number << std::endl;
		std::cout << "Hire Date: " << hireDate << std::endl;
	}
	//setters
	void setName(std::string name) { this->name = name; }
	void setNumber(int number) { this->number = number; }
	void setHireDate(std::string hireDate) { this->hireDate = hireDate; }
};

class ProductionWorker : public Employee
{
private:
	int shift;
	double hourlyPay;
public:
	//constructors
	ProductionWorker()
		: Employee() 
	{
		shift = 0;
		hourlyPay = 0.0;
	}

	//getters
	int getShift() { return shift; }
	double getHourlyPay() { return hourlyPay; }

	//setters
	void setShift(int shift) { this->shift = shift; }
	void setHourlyPay(double hourlyPay) { this->hourlyPay = hourlyPay; }

	//printME
	virtual void printMe() 
	{
		std::cout << "Name:" << this->getName() << std::endl;
		std::cout << "Employee Number: " << this->getNumber() << std::endl;
		std::cout << "Hire Date: " << this->getHireDate() << std::endl;
		std::cout << "Shift: " << shift << std::endl;
		std::cout << "Hourly Pay: " << hourlyPay << std::endl;
	}
};

class ShiftSupervisor : public Employee
{
private:
	double salary;
	double bonus;
public:
	//constructor
	ShiftSupervisor()
		: Employee() 
	{
		salary = 0.0;
		bonus = 0.0;
	}
	//getters
	double getSalary() { return salary; }
	double getBonus() { return bonus; }
	//setters
	void setSalary(double salary) { this->salary = salary; }
	void setBonus(double bonus) { this->bonus = bonus; }
	virtual void printMe()
	{
		std::cout << "Name:" << this->getName() << std::endl;
		std::cout << "Employee Number: " << this->getNumber() << std::endl;
		std::cout << "Hire Date: " << this->getHireDate() << std::endl;
		std::cout << "Salary: " << salary << std::endl;
		std::cout << "Bonus: " << bonus << std::endl;
	}
};

class TeamLeader : public ProductionWorker
{
private:
	double monthlyBonus;
	double minTrainHours;
	double trainHoursAtt;
public:
	//constructor
	TeamLeader()
		: ProductionWorker() 
	{
		monthlyBonus = 0.0;
		minTrainHours = 0.0;
		trainHoursAtt = 0.0;
	}
	
	//getter
	double getMonthlyBonus() { return monthlyBonus; }
	double getMinTrainHours() { return minTrainHours; }
	double getTrainHoursAtt() { return trainHoursAtt; }

	//setter
	void setMonthlyBonus(double monthlyBonus) { this->monthlyBonus = monthlyBonus; }
	void setMinTrainHours(double minTrainHours) { this->minTrainHours = minTrainHours; }
	void setTrainHoursAtt(double trainHoursAtt) { this->trainHoursAtt = trainHoursAtt; }

	//printMe
	virtual void printMe()
	{
		std::cout << "Name:" << this->getName() << std::endl;
		std::cout << "Employee Number: " << this->getNumber() << std::endl;
		std::cout << "Hire Date: " << this->getHireDate() << std::endl;
		std::cout << "Shift: " << this->getShift() << std::endl;
		std::cout << "Hourly Pay: " << this->getHourlyPay() << std::endl;
		std::cout << "Monthly Bonus: " << monthlyBonus << std::endl;
		std::cout << "Minimum Training Hours : " << minTrainHours << std::endl;
		std::cout << "Training Hours Attended: " << trainHoursAtt << std::endl;
	}

};

#endif