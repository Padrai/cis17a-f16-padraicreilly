#include "EmployeeTypes.h"



void waitToQuit()
{
	char quit = 'a';
	while (quit != 'q')
	{
		std::cout << "enter 'q' to quit." << std::endl;
		std::cin >> quit;
	}
}

int main() 
{
	std::cout << "////////////EMPLOYEE BASE CLASS//////////////" << std::endl;
	Employee A;
	A.setName("Harry Potter");
	A.setHireDate("11/09/2016");
	A.setNumber(123); 
	A.printMe();

	std::cout << "///////PRODUCTION WORKER CLASS///////////////////" << std::endl;
	ProductionWorker B;
	B.setName("Ron Weasley");
	B.setHireDate("12/25/2002");
	B.setNumber(456);
	B.setShift(1);
	B.setHourlyPay(13.0);
	B.printMe();


	std::cout << "////////////SHIFT SUPERVISOR//////////////" << std::endl;
	ShiftSupervisor C;
	C.setName("Hermione Granger");
	C.setHireDate("01/02/2005");
	C.setNumber(789);
	C.setBonus(120.0);
	C.setSalary(20000.0);

	C.printMe();

	std::cout << "//////////////TEAM LEADER////////////" << std::endl;
	TeamLeader D;
	D.setName("Albus Dumbledore");
	D.setHireDate("07/09/1952");
	D.setNumber(54321);
	D.setShift(2);
	D.setHourlyPay(22.0);
	D.setMonthlyBonus(220.0);
	D.setMinTrainHours(5.5);
	D.setTrainHoursAtt(2.1);

	D.printMe();

	waitToQuit();
	return 0;
}

