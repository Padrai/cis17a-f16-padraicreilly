/*
* Name: Padraic Reilly
* Student ID: 2667697
* Date: 11/20/16
* HW: 8
* Problem: LinkedList:	My example is a forward-only linked list of students(comprised of a name and GPA).
						I wasn't sure about how to implement the insertNode(), whether to be passing it an
						index value to try to insert at, or whether to just insert behind the last one, or 
						what, exactly. I went with the index value, and basically just first determined if
						the desiredIndex it is in range, then iterating a variable until I know I am at 
						that entry in the linkedList. I had another version that didn't rely on iterating 
						a counter variable but I couldn't get it working perfectly.
						
* I certify this is my own work and code. 


*/
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

class StudentList 
{
	private:
		struct List_Node 
		{
			//data
			string name;
			double GPA;
			//link
			List_Node* next;
		};
		List_Node * head;
	public:
		//Constructor
		StudentList();
		//Destructor
		~StudentList();
		//Linked List Ops
		void appendNode(string, double);
		bool insertNode(string, double, int);
		void deleteNode(string);
		void displayList() const;
};


StudentList::StudentList() { head = nullptr;}

StudentList::~StudentList()
{
	List_Node* deleter;
	List_Node* nextToDelete;

	if (head == nullptr)  {cout << "Array Already empty!" << endl; }
	else 
	{
		deleter = head;
		cout<<endl;
		do{
		    
		    cout<<"Deconstructing "<<deleter->name<<endl;
			nextToDelete = deleter->next;
			delete deleter;
			deleter = nextToDelete;
			

		}while (nextToDelete != nullptr);
	    
	}
	cout<<endl<<"---ROSTER CLEARED---" << endl;
}

void StudentList::appendNode(string name, double GPA)
{
	List_Node* newNode; //allocate new node
	List_Node* appNodePtr; //pointer for iterating thru list

	newNode = new List_Node;
	newNode->name = name;
	newNode->GPA = GPA;

	if (!head) { head = newNode; }//if there are no other elements, then make this one the first one, or "head"
	else 
	{
		appNodePtr = head;
		while (appNodePtr->next)
		{
			appNodePtr = appNodePtr->next;
		}
		appNodePtr->next = newNode;
	}
}

bool StudentList::insertNode(string name, double GPA, int indexDesired)
{
	// make the newnode(with name,GPA)
	List_Node * newNode = new List_Node;
	newNode->name = name;
	newNode->GPA = GPA;
	
	//Find size 
	int size = 0;
	List_Node * nodePtr;
	nodePtr = head;
	while(nodePtr != nullptr)
	{
		size++; //increment
		nodePtr = nodePtr->next; //move forward
	}
	cout<< "Size = " <<size<<endl; 
	nodePtr = head; //reset nodePtr;
	
	if (!(indexDesired >= 0 && indexDesired <= size)){/*cout<<"Destination invalid!"<<endl;*/ return false;}
	else
	{
		//cout<<"Insert destiniation approved"<<endl;
		for (int i = 0; i <= indexDesired; i++)
		{
			if (i + 1 == indexDesired)
			{
				newNode ->next = nodePtr->next;
				nodePtr->next = newNode;
				
			}
			nodePtr = nodePtr->next;
		}
		return true;
	}
}

void StudentList::displayList() const
{
    cout<<setprecision(2);
	List_Node* dispNodePtr;
	dispNodePtr = head;
	cout <<endl<< "Current roster: " << endl;
	while (dispNodePtr)
	{
		cout << "Name: " << dispNodePtr->name << "   GPA: " << dispNodePtr->GPA << endl;
		dispNodePtr = dispNodePtr->next;
	}
	cout<<"End of Roster"<<endl<<endl;
}

void StudentList::deleteNode(string name) 
{
	bool found = false;
	List_Node* delNodePtr;
	delNodePtr = head;
	List_Node* previousNode;
	
	if (!head) { return;}//if empty
	//find the one you want and always deallocate
	if (head->name == name)//if it is the front case
	{
		found = true;
		cout<<"Deleting "<<head->name<<endl;
		delNodePtr = head->next; //delNodePtr is passed second value
		delete head; //deallocate head
		head = delNodePtr; //set the head to the (originally) second value
	}
	
	else //middle or end cases: Link previous and next, set end  of the chain to be nullptr
	{
		delNodePtr = head; //set reader to beginning
		previousNode = nullptr;
		while (delNodePtr != nullptr && delNodePtr->name != name) //skip all non relevant ones
		{	
		    
		
			previousNode = delNodePtr;
			delNodePtr = delNodePtr->next;
		}
		if (delNodePtr)
		{
			found = true;
			cout<<"Deleting "<< delNodePtr->name <<endl;
			previousNode->next = delNodePtr->next;
			delete delNodePtr;
		}
	}
if (!found)	{cout<<name <<" not found! Nothing deleted."<<endl;}
}

int main()
{
	
	StudentList Hogwarts;
	
	cout<<"Testing Adding Function."<<endl;
	
	Hogwarts.appendNode("Harry", 3.1);
	Hogwarts.appendNode("Ron", 3.3);
	Hogwarts.appendNode("Hermione", 4.2);
	Hogwarts.appendNode("Malfoy", 3.7);
	Hogwarts.appendNode("Crab", 2.6);
	Hogwarts.appendNode("Goyle", 1.9);
	Hogwarts.appendNode("Tom Riddle", 4.0);
	
	cout<<"Testing Display Function."<<endl;	
	Hogwarts.displayList();
	
	cout<<"Testing Delete Function."<<endl;
	Hogwarts.deleteNode("Harry"); //testing case: if match is FIRST in linked-list
	Hogwarts.deleteNode("Hermione");//testing case: if match is MIDDLE in linked-list
	Hogwarts.deleteNode("Tom Riddle");//testing case: if match is LAST in linked-list
	Hogwarts.deleteNode("Justin Bieber");
	
	Hogwarts.displayList();
	
	cout<<"Testing Insert Function."<<endl;
	bool worked = Hogwarts.insertNode("Voldermort" , 3.5 , 3);
	if(worked){Hogwarts.displayList();}
	
	worked = Hogwarts.insertNode("Brianna", 4.1, 5);
	
	if(worked){Hogwarts.displayList();}
	
	return 0;
}