/*
* Name: PADRAIC REILLY	
* Student ID: 2667697
* Date: 9/21 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* Lab: 1
* Problem:2
* I certify this is my own work and code
*/

#include <iostream>
#include <ctime>
using namespace std;

int main()
{ 
    //part 2
    srand(time(0));
    int * arr;
    int size;
    cout<<"Enter size:"<<endl;
    cin>>size;
    
    arr = new int[size];
    
    cout<<"Junk values in array of size "<<size<<":"<<endl;
    for(int i = 0; i<size; i++)
    {
        cout <<i + 1 <<". "<<arr[i]<<endl;
    }
    cout<<endl;
    
    cout<<"Filling with random values in range of 0 -> 100. "<<endl;
    for (int i = 0; i<size; i++)
    {
        arr[i] = rand()% 101;
    }
    
     cout<<"Random values in array of size "<<size<<":"<<endl;
    for(int i = 0; i<size; i++)
    {
        cout <<i + 1 <<". "<<arr[i]<<endl;
    }
    
    delete[] arr;
   
    
    return 0;
}