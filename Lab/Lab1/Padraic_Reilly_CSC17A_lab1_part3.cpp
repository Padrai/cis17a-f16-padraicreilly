/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* LAb: 1
* Problem:3
* I certify this is my own work and code
*/
#include <iostream>
#include <ctime>
using namespace std;

void swap(double a, double b);

int main()
{ 
  //Part 3
 int a, b;
 cout<< "Enter 1st value: ";
 cin>> a;
 
 cout<<"a: "<<a<<endl<<endl;
 cout<< "Enter 2nd value: "<<endl;
 cin>> b;
 cout<<"b: "<<b<<endl<<endl;
 cout<<endl<<"The values will now be swapped."<<endl;
 swap(a,b);
 
 cout<<"a: "<<a<<endl;
 cout<<"b: "<<b<<endl;
 
    return 0;
}

void swap(double a, double b)
{
    double temp = a;
    a = b;
    b = temp;
}