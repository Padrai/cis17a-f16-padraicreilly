#include <iostream>
#include <ctime>
/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* LAb: 1
* Problem:4
* I certify this is my own work and code
*/

int doSomething(int * x, int * y);

int main()
{ 
    //PART 4
    int * x = new int;
    int * y = new int;
    std::cout<<"You will be multiplying two values by 10, swapping them and adding them together"<<std::endl;
    std:: cout<<"enter value of x"<<std::endl;
    std:: cin>>*x;
    std::cout<<"enter value of y"<<std::endl;
    std::cin>>*y;
    
    std::cout << doSomething(x,y);
    
    return 0;
}

int doSomething(int * x, int * y)
{
   int temp = *x;
   *x = *y * 10;
   *y = temp * 10;
   return *x + *y;
}

