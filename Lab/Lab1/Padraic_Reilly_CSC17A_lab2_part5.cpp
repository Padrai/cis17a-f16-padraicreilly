/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* LAb: 1
* Problem:5
* I certify this is my own work and code
*/
#include <iostream>
#include <ctime>
#include <cmath>

double fallingDistance(double t);

int main()
{   
    //PART 5
    for(int i = 1; i<11; i++){std::cout<<"When t = "<<i<<", the object has fallen "<< fallingDistance(i)<<" meters"<<std::endl;}
    return 0;
}


double fallingDistance(double t)
{
    const double g = 9.8;
    return (9.8 / 2)*pow(t,2);

}
