/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* LAb: 1
* Problem:6
* I certify this is my own work and code
*/
#include <iostream>
#include <cmath>

double presentValue(double f, double r, double n);

int main()
{   
    //PART 6
    double r,n,f;
    std::cout<<"Enter the Final amount: "<<std::endl;
    std::cin>>f;
    std::cout<<"Enter the interest rate as a percentage (i.e. 8): "<<std::endl;
    std::cin>>r;
    std::cout<<"Enter the number of years:"<<std::endl;
    std::cin>>n;
    
    std::cout<<"You need to put this much in the bank today to reach your goal: $"<<presentValue(f,r,n);
}

double presentValue(double f, double r, double n)
{   
    double p;
    p = f/(pow((1+(r/100.0)),n));
     return p;
}
