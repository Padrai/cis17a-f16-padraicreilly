/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* LAb: 1
* Problem:7
* I certify this is my own work and code
*/
#include <iostream>
#include <cmath>
#include <ctime>


double findMedian(int * array, int size);

int main()

{
    //PART 7                                                                                            
    srand(time(0));
    int size = rand()%12 + 4;
    int * arr = new int[size];
    for(int i = 0; i <size; i++){arr[i] = rand() % 100;}
    std::cout<<"Here is a random generated array of random size "<<size<<std::endl;
    for(int i = 0; i <size; i++){std::cout<<arr[i]<<std::endl;}
   std::cout<<std::endl;
    double median = findMedian(arr, size);
    std::cout<<"MEDIAN: "<<median;
    
}

double findMedian(int * array, int size)
{
    
    //sort
    bool swap;
    do
    {
        swap = false;
        for(int i = 0; i<size-1;i++)
        {
            if(array[i]<array[i + 1])
            {
                swap = true;
                int temp = array[i];
                array[i] = array[i + 1];
                array[i+1] = temp;
            }
        }
    }
    while(swap);
    
    //
    std::cout<<std::endl<<std::endl<<"Here is sorted array"<<std::endl;
    for(int i = 0; i <size; i++){std::cout<<array[i]<<std::endl;}
    std::cout<<std::endl;
    //find median
     double median;

    if (size%2 ==0)
    { 
        median = static_cast<double>(((array[size/2 - 1] + array[(size/2) ])))/2.0; 
        return median;
    }
    
    else
    {
        median = static_cast<double>(array[size/2]);
        return median;
        
    }  

    
}