/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22 (ADDED HEADER, IT WAS TURNED IN ON TIME)
* LAb: 1
* Problem:8
* I certify this is my own work and code
*/
#include <iostream>
#include <cmath>
#include <ctime>


int * arrExpand(int * origArr, int &size);


int main()  
{
    
    //PART 8
    
    
    //generate test value range/count;
    srand(time(0));
    int size = rand()%10 + 2;
    int range = rand()%50 + 1;
    std::cout<<"Here is your matrix of random size "<<size<< " and of random range "<<range<<std::endl;
 
    //generate array, fill with test values, print
    int * anyArray = new int[size];
    for(int i = 0;  i <size;  i++){anyArray[i] = rand()%range + 1;}
    for(int i = 0;  i <size;  i++){std::cout<<anyArray[i]<<std::endl;}
 
    //test array expander
    std::cout<<std::endl<<std::endl<<"Testing array expander:"<<std::endl;
    anyArray = arrExpand(anyArray,size);
    std::cout<<"Here is your mew matrix of size "<<size<<"."<<std::endl;
    for(int i = 0;  i <size;  i++)
    {   
        std::cout<<anyArray[i]<<std::endl;
        if(i == size/2 - 1){std::cout<<"---------------------------"<<std::endl;}
    }
  
    
    
    return 0;
}

int * arrExpand(int * origArr, int &size) 
{int * newArr = new int[size * 2];
for (int i = 0; i<size; i++){newArr[i] = origArr[i];}
for (int i = size + 1; i < 2*size; i++){newArr[i]=0;}
size *= 2;

    return newArr;
}