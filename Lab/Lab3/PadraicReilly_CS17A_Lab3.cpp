
/* 7. Customer Accounts
Write a program that uses a structure to store the following data about a customer
account:
Name
Address
City, State, and ZIP
Telephone Number
Account Balance
Date of Last Payment

The program should use an array of at least 10 structures. It should let the user enter
data into the array, change the contents of any element, and display all the data stored
in the array. The program should have a menu-driven user interface.
Input Validation: When the data for a new account is entered, be sure the user enters
data for all the fields. No negative account balances should be entered.*/




    
// creates new array of size + 1 and copies over old stuff
//attributes at size  + 1 are initialized blank or 0;
//deletes old array
//returns new address of new Account array



//void editInfomation(customerAccount * acct, int index);
//Edits attributes at index # of Account arry

//void printAnAccount(const customerAccount * acct, int index);
// print all Attributes of account located at index of Accounts Array

//void printAllAccounts(const customerAccount * acct);
//loops while calling printAnAccount() and increments the index passed to it
#include <iostream>
#include <string>
using namespace std;
struct customerAccount
{
    string name;
   
    string streetNumAndStreet;
    string city;
    string state;
    int zipCode;
    int phoneNumber;
    double accountBalance;
    string dateOfLastPayment;
};
typedef struct customerAccount customerAccount;


customerAccount * addAccount(customerAccount * acct, int &size);
customerAccount * deleteAccount(customerAccount* acct, int &size);
void viewEntries(customerAccount * acct,const int size);
void editInformation(customerAccount * acct, int size);
void searchFunction(customerAccount * acct, int size);
void fillWholeCustomerProfile(customerAccount * acct, int size);

int main()
{   int size = 0;
    customerAccount * accountList = new customerAccount[size];
    bool quit = false;
    while (!quit)
    {   char userInput;
        cout<<"Want to add an account (a), search for an account by NAME (s), delete an account (d), view existing entries (v),edit an existing account (e), or quit (q)?"<<endl;
        cin>>userInput;
        if(userInput =='a'){accountList = addAccount(accountList,size);}
        else  if(userInput == 'd') {accountList = deleteAccount(accountList, size);}
        else if (userInput == 'v'){viewEntries(accountList, size);}
        else if (userInput == 's'){searchFunction(accountList, size);}
        else if(userInput== 'e'){editInformation(accountList, size);}
        else if (userInput == 'f'){fillWholeCustomerProfile(accountList, size);}
        else if(userInput == 'q'){quit = true;}
        else {cout<<"Invalid Entry"<<endl;}
        
    }
    return 0;
}

customerAccount * addAccount(customerAccount * acct, int &size)
{
    customerAccount * temp = new customerAccount[size + 1];
    for(int i = 0; i<size; i++){temp[i] = acct[i];}
    temp[size].name = "";
    temp[size].streetNumAndStreet = "";
    temp[size].city = "";
    temp[size].state = "";
    temp[size].zipCode = 0;
    temp[size].phoneNumber = 0;
    temp[size].accountBalance = 0.0;
    temp[size].dateOfLastPayment = "";
    
    size++;
    return temp;
}   

customerAccount* deleteAccount(customerAccount * acct, int &size)
    {customerAccount * temp = new customerAccount[size - 1];
    
    int num2delete;
    cout<<"which account did you want to delete? Enter a number"<<endl;
    do{cin>>num2delete;}while(--num2delete >= size || num2delete<0);

    int j = 0;
    for(int i = 0; i<size; i++)
    {
        if (i != num2delete) {temp[j] = acct[i]; j++;}
    }
    size--;
    return temp;
}   

void viewEntries(customerAccount * acct,const int size)
{   
    cout<<"Viewing Entries:"<<endl;
    for (int i = 0; i < size; i++)
    {
        cout<<i+1<<"a "<<"Name: "<<acct[i].name<<endl;
        cout<<i+1<<"b "<<"Address: "<<acct[i].streetNumAndStreet<<endl;
        cout<<i+1<<"c "<<"City: "<<acct[i].city<<endl;
        cout<<i+1<<"d "<<"State: "<<acct[i].state<<endl;
        cout<<i+1<<"e "<<"Zip Code: "<<acct[i].zipCode<<endl;
        cout<<i+1<<"f "<<"Phone Number:"<<acct[i].phoneNumber<<endl;
        cout<<i+1<<"g "<<"Account Balance: "<<acct[i].accountBalance<<endl;
        cout<<i+1<<"h "<<"Date of Last Payment: "<<acct[i].dateOfLastPayment<<endl<<endl;
    }
    cout<<"END OF RECORDS."<<endl;
}

void editInformation(customerAccount * acct, int size)
{   
    string userInput;
    double dUserInput;
    int iUserInput;
    int index =0;
    char attributeSelect;
    cout<<"Which number entry would you like to edit?"<<endl;
    do {cin>>index;}while(index--<0 || index >= size); 
    cout<<"Which letter entry would you like to edit?"<<endl; 
    bool loop = true;
    while(loop)
    {   cin.ignore();
        cin >> attributeSelect;
        if (attributeSelect == 'a'||attributeSelect == 'b'||attributeSelect == 'c'||attributeSelect == 'd'||attributeSelect == 'e'||attributeSelect == 'f'||attributeSelect == 'g'||attributeSelect == 'h')
        {loop = false;}
    };
   cin.ignore();
    if (attributeSelect == 'a')
    {
        cout<<"Enter the NAME you want on the file: "<<endl;
        getline(cin, userInput);
          cin.ignore();
        acct[index].name = userInput;
    }
    else if (attributeSelect == 'b')
    {
        cout<<"Enter the ADDRESS you want on the file(Example \"5658 Allendale Ave\"): "<<endl;
        getline(cin, userInput);
          cin.ignore();
        acct[index].streetNumAndStreet= userInput;
    }
    else if (attributeSelect == 'c')
    {
        cout<<"Enter the CITY you want on the file: "<<endl;
        getline(cin, userInput);
          cin.ignore();
        acct[index].city = userInput;
    }
    else if (attributeSelect == 'd')
    {
        cout<<"Enter the STATE you want on the file: "<<endl;
        getline(cin, userInput);
        cin.ignore();
        acct[index].state = userInput;
    }
    else if (attributeSelect == 'e')
    {
        cout<<"Enter the ZIP CODE you want on the file: "<<endl;
        cin>> iUserInput;
          cin.ignore();
        acct[index].zipCode = iUserInput;
    }
    else if (attributeSelect == 'f')
    {
        cout<<"Enter the PHONE NUMBER you want on the file (1 number with no spaces): "<<endl;
        cin>>iUserInput;
        cin.ignore();
        acct[index].phoneNumber = iUserInput;
    }
    else if (attributeSelect == 'g')
    {
        cout<<"Enter the ACCOUNT BALANCE you want on the file: (Enter as a floating point number): "<<endl;
        cin>> dUserInput;
          cin.ignore();
        acct[index].accountBalance = dUserInput;
    }
    else if (attributeSelect == 'h')
    {
        cout<<"Enter the DATE OF LAST PAYMENT you want on the file: example (5 16 2016)"<<endl;
        getline(cin, userInput);
          cin.ignore();
        acct[index].dateOfLastPayment = userInput;
    }
    
    
    
    
   
}

void fillWholeCustomerProfile(customerAccount * acct, int size)
{   
    string userInput;
    double dUserInput;
    int iUserInput;
    int index;
    
    cout<<"Which number entry would you like to fill from start to finish? "<<endl;
    do {cin>>index;}while(index--<0 || index >= size); 
    
    cout<<"Enter the NAME you want on the file: "<<endl;
    cin.ignore();
    getline(cin, userInput);
    acct[index].name = userInput;
    
    cout<<"Enter the ADDRESS you want on the file(Example \"5658 Allendale Ave\"): "<<endl;
    cin.ignore();
    getline(cin, userInput);
    acct[index].streetNumAndStreet = userInput;
    
    cout<<"Enter the CITY you want on the file: "<<endl;
    cin.ignore();
    getline(cin, userInput);
    acct[index].city = userInput;
    
    cout<<"Enter the STATE you want on the file: "<<endl;
    cin.ignore();
    getline(cin, userInput);
    acct[index].state = userInput;
    
    cout<<"Enter the ZIP CODE you want on the file: "<<endl;
    cin>> iUserInput;
    acct[index].zipCode = iUserInput;
    
    cout<<"Enter the PHONE NUMBER you want on the file (1 number with no spaces): "<<endl;
    cin>>iUserInput;
    acct[index].phoneNumber = iUserInput;
    
    cout<<"Enter the ACCOUNT BALANCE you want on the file: (Enter as a floating point number): "<<endl;
    cin>> dUserInput;
    acct[index].accountBalance = dUserInput;
    
    cout<<"Enter the DATE OF LAST PAYMENT you want on the file: example (5 16 2016)"<<endl;
    cin.ignore();
    getline(cin, userInput);
    
    acct[index].dateOfLastPayment = userInput;
    
        
     
     
    
}

void searchFunction(customerAccount * acct, int size)
{   customerAccount * matches = new customerAccount[size];
    int k = 0;
    string searchTerm;
    cout<<"Enter your search term"<<endl;
    cin.ignore();
    getline (cin,searchTerm);
    
    
    for(int i = 0; i<size;i++)
    {
        if ( acct[i].name.find(searchTerm) != -1 ){matches[k] = acct[i]; k++;}
    }
    viewEntries(matches, k);
    
}