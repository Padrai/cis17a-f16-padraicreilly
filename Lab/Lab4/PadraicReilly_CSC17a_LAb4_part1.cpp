/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22/16
* Lab: 4
* Problem: 13.5 , pg 803
* I certify this is my own work and code
*/
/*5. RetailItem Class
Write a class named RetailItem that holds data about an item in a retail store. The
class should have the following member variables:
• description . A string that holds a brief description of the item.
• unitsOnHand . An int that holds the number of units currently in inventory.
• price . A double that holds the item’s retail price.

Write a constructor that accepts arguments for each member variable, appropriate
mutator functions that store values in these member variables, and accessor functions
that return the values in these member variables. Once you have written the class, write
a separate program that creates three RetailItem objects and stores the following data
in them.
*/

#include <iostream>
#include <string>

using namespace std;

class RetailItem 
{   
    private:
    string description;
    int unitsOnHand;
    double price;
    
    public:
    // memeber functions to edit values
    void changePrice(double newPrice);
    void changeCount(int newCount);
    void changeDescription(string newDescription);
    //member functions to access(return) values
    double accessPrice();
    int accessCount();
    string accessDescription();
    //constructor
    RetailItem(string, int , double);
};
//constructor declaration
RetailItem::RetailItem(string descr, int cnt, double prc)
{
    description = descr;
    unitsOnHand = cnt;
    price = prc;
    cout<<"Description: "<< description<<endl;
    cout<<"Units on Hand: "<<unitsOnHand<<endl;
    cout<<"Price: $"<<price<<endl<<endl;
}
//edit functions declaration
void RetailItem::changePrice(double newPrice){price =  newPrice;}
void RetailItem::changeCount(int newCount){unitsOnHand = newCount;}
void RetailItem::changeDescription(string newDescription){description =  newDescription;}

double RetailItem:: accessPrice(){return price;}
int RetailItem:: accessCount(){return unitsOnHand;}
string RetailItem::accessDescription(){return description;}

int main()
{
    
    
    RetailItem A("Jacket ",12,59.95);
    RetailItem B("Designer Jeans",40,34.95);
    RetailItem C("Shirt",20,24.95);
    
    return 0;
}