/*
* Name: PADRAIC REILLY
* Student ID: 2667697
* Date: 9/22/16
* Lab: 4
* Problem: 13.7 , pg 803
* I certify this is my own work and code
*/

/*
Design a TestScores class that has member variables to hold three test scores. The
class should have a constructor, accessor, and mutator functions for the test score fields
and a member function that returns the average of the test scores. Demonstrate the
class by writing a separate program that creates an instance of the class. The program
should ask the user to enter three test scores, which are stored in the TestScores
object. Then the program should display the average of the scores, as reported by the
TestScores object.
*/

#include <iostream>

using namespace std;

class TestScores
{
    private: 
    int score1, score2, score3;
    double avg;
    
    public:
    TestScores(int firstScore, int secondScore, int thirdScore);
    
    int accessScore1();
    int accessScore2();
    int accessScore3();
    double accessAvg();
    
    void editScore1(int newScore);
    void editScore2(int newScore);
    void editScore3(int newScore);
    
    void setAverage();
    
    
};
//member functions
TestScores::TestScores(int firstScore, int secondScore, int thirdScore)
{
    score1 = firstScore; 
    score2 = secondScore; 
    score3 = thirdScore;
}
//access fucntions
int TestScores::accessScore1(){return score1;}
int TestScores::accessScore2(){return score2;}
int TestScores::accessScore3(){return score3;}
double TestScores::accessAvg(){return avg;}

void TestScores::editScore1(int newScore){score1 = newScore;}
void TestScores::editScore2(int newScore){score2 = newScore;}
void TestScores::editScore3(int newScore){score3 = newScore;}

void TestScores::setAverage(){avg = static_cast<double>((score1+score2+score3)/3.0);}

int main()
{  
    cout<<"Welcome to the score averager (Using CLASSES)."<<endl;
    int firstScore, secondScore, thirdScore;
    do{
        cout<<"Enter your first Score:"<<endl;
        cin>> firstScore;
    }while(firstScore>100 || firstScore<0);
     do{
        cout<<"Enter your second Score:"<<endl;
        cin>> secondScore;
    }while(secondScore>100 || secondScore<0);
     do{
        cout<<"Enter your third Score:"<<endl;
        cin>> thirdScore;
    }while(thirdScore>100 || thirdScore<0);
    
    cout<<"Okay, all scores recieved. Creating instance of testScores class...";
    TestScores instance(firstScore,secondScore,thirdScore);
    cout<<"done."<<endl;
    cout<<"Calculating average..."<<endl;
    instance.setAverage();
    cout<<"Your average is : "<<instance.accessAvg()<<endl;
    
    return 0;
}