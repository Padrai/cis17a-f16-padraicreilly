#include "Student.h"
#include <string>
#include <iostream>
//default
Student::Student()
{
 name = "";
 id = 0;
 testSize = 1;
 tests = NULL;
} 
//copy
Student::Student(const Student& orig)
{
    //assign orig values to newly constructed instance
    cout<<"Using copy constructor!"<<endl;
    this->name = orig.name;
    this->id = orig.id;
    this->testSize = orig.testSize;
    this->tests = new int[testSize];
    for (int i = 0; i<testSize; i++){this->tests[i] = orig.tests[i];}
} 

Student::Student(int size){}

//destructor
Student::~Student(){cout<<"Destructor running"<<endl; delete[] tests;} 
//Operator Overloading
//asignment
const Student Student::operator=(const Student& right)
{
    if(this == &right){return *this;} 
    delete[] tests;
    this->name = right.name;
    this->id = right.id;
    this-> testSize = right.testSize;
    
    this -> tests = new int[testSize];
    
    for(int i = 0;  i < testSize; i++){this->tests[i] = right.tests[i];}
}
//prefix
Student Student::operator++(){}
//postfix
Student Student::operator++(int){}
//subscript[] overloading
Student Student::operator[](int loc){}
//getters/setters
string Student::getName(){}
void Student::setName(string){}
int Student::getId(){}
void Student::setId(int){}