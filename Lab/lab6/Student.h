#ifndef STUDENT_H
#define STUDENT_H

#include<string>
using namespace std;

class Student
{
        private:
        string name;
        int id;
        int testSize;
        int* tests;
    public: 
        //big 3 
        Student(); //default
        Student(const Student& orig); //copy
        Student(int size); 
        ~Student(); //destructor
    
    //Operator Overloading
    //asignment
    const Student operator=(const Student&);
    //prefix
    Student operator++();
    //postfix
    Student operator++(int);
    //subscript[] overloading
    Student operator[](int loc);
    
    //getters/setters
    string getName();
    void setName(string);
    int getId();
    void setId(int);
};

#endif