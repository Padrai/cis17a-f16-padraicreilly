#include "Month.h"
#include <ostream>

using namespace std;

Month::Month(){num = 1; setName(num);} // Default, Jan 1 plz
Month::Month(string name, int num){this->name = name; this -> num = num;}
Month::Month(string moNa){name = moNa; setNum(name);} //overloaded constructor, set name & num appropriately
Month::Month(int moNu){num = moNu; setName(num);} //overload constructor, set name and num appropriately
        
//GETTERS
string Month::getName(){return name;}
int Month::getNum(){return num;}
        

//SETTERS

void Month::setName(string name){this->name = name;}


void Month::setName(int num) //(overloaded setName to to do so using a months number)
{
    if (num == 1){name = "January";}
    else if (num == 2){name = "February";}
    else if (num == 3){name = "March";}
    else if (num == 4){name = "April";}
    else if (num == 5){name = "May";}
    else if (num == 6){name = "June";}
    else if (num == 7){name = "July";}
    else if (num == 8){name = "August";}
    else if (num == 9){name = "September";}
    else if (num == 10){name = "October";}
    else if (num == 11){name = "November";}
    else if (num == 12){name = "December";}
    this-> num = num;
}

void Month::setNum(int num){this->num = num;}

void Month::setNum(string name)
{
    if (name == "January"){num = 1;name = "January";}
    else if (name == "February"||name == "february"||name == "Feb"||name == "feb"){num = 2;this->name = "February";}
    else if (name == "March"||name == "March"||name == "Mar"||name == "mar"){num = 3;this->name = "March";}
    else if (name == "April"||name == "april"||name == "Apr"||name == "apr"){num = 4;this->name = "April";}
    else if (name == "May"||name == "may"){num = 5;this->name = "May";}
    else if (name == "June"||name == "june"){num = 6;this->name ="June";}
    else if (name == "July"||name == "july"){num = 7;this->name = "July";}
    else if (name == "August"||name == "august"||name == "aug"||name == "Aug"){num = 8;this->name = "August";}
    else if (name == "September"||name == "september"||name == "Sept"||name == "sep"){num = 9;this->name = "September";}
    else if (name == "October"||name == "october"||name == "oct"||name == "Oct"){num = 10;this->name = "October";}
    else if (name == "November"||name == "november"||name == "Nov"||name == "nov"){num = 11;this->name = "November";}
    else if (name == "December"||name == "december"||name == "Dec"||name == "dec"){num = 12;this->name = "December";}
}

//OPERATOR OVERLOAD

//prefix
Month Month::operator++()
{
   ++num;
   if(num> 12){num = 1;}
   this->setName(num);
   return Month(name, num);
}


Month Month::operator--()
{
    --num;
    if (num<1){num = 12;}
    this->setName(num);
    return Month(name, num);
}

//postfix
Month Month::operator++(int)//dummy variable differentiates postfix and prefix
{
    Month T;
    T.name = name;
    T.num = num;
    
    num++;
    if(num > 12){num = 1;}
    this->setName(num);
    
    return T;
}

Month Month::operator--(int)//dummy variable differentiates postfix and prefix
{
    Month T;
    T.name = name;
    T.num = num;
    
    --num;
    if (num < 1){num = 12;}
    this->setName(num);
    
    return T;
    
} 

std::ostream& operator<< (std::ostream& output, const Month& A)
{
    int a  = A.num;
    
    output<<"Month: "<<A.name<<"  Number: "<<to_string(A.num);//add number converted to string
    return output;
}


std::istream& operator>>(istream &input, Month &A)
{
    input>>A.name;//stoi(A.getNum());
    A.setNum(A.name);
    return input;
}
