#ifndef MONTH_H
#define MONTH_H

#include <string>
using namespace std;
class Month
{
    private:
        string name;
        int num;
    public:
        Month(); // Default, Jan 1 plz
        Month(string name, int num);
        Month(string); //overloaded constructor, set name & num appropriately
        Month(int); //overloa constructor, set name and num appropriately
        
        //getters
        string getName();
        int getNum();
        
        //setters
        void setName(string);
        void setName(int num);
        void setNum(int);
        void setNum(string);
        
        //operator overload
        Month operator++();
        Month operator++(int);//dummy variable differentiates postfix and prefix
        Month operator--();
        Month operator--(int);//dummy variable differentiates postfix and prefix
        
        //iostream << >> operator overload
        friend std::ostream& operator<< (std::ostream&, const Month&);
        friend std::istream& operator>>(std::istream&, Month&);
};



#endif

/*
overload cout ’s << operator 
and cin ’s >> operator to work with the Month class. 
Demonstrate the class in a program.
*/