
#include "Month.cpp"
#include <iostream>
#include <ctime>
#include <stdlib.h>
using namespace std;

void printMonth(Month right)
{
    cout<<"Month: "<<right.getName()<<", number "<<right.getNum()<<endl;
}




int main()
{
    srand(time(0));
    cout<<"Postfix Increment Operator:"<<endl;
    Month A;
    for (int i = 0; i<3; i++)
    {
        printMonth(A++);
    }
    cout<<endl;
    
    cout<<"Prefix Increment Operator:"<<endl;
    Month B;
    for (int i = 0; i < 3; i++)
    {
        printMonth(++B);
    }
    cout<<endl;
    
    cout<<"Increment rollover test. Starting at 12, incrementing 1 time." <<endl;
    Month C;
    C.setNum(rand()%12 + 1);
    C.setName(12);
    printMonth(C);
    C++; 
    printMonth(C);
    
    cout<<endl<<"Postfix decrement operator test:  "<<endl;
    Month D;
    for (int i = 0; i < 3; i++)
    {
        printMonth(D--);
    }
    
     cout<<endl<<"Prefix decrement operator test:  "<<endl;
    Month E;
    for (int i = 0; i < 3; i++)
    {
        printMonth(--E);
    }
    
    
    cout<<endl<<"Decrement rollover test. Starting at 1, decrementing 1 time." <<endl;
    Month F;
    F.setNum(1);
    F.setName(1);
    printMonth(F);
    F--; 
    printMonth(F);
    
    cout<<endl<<"cout statement test: "<<endl;
    Month G(rand()%12 + 1);
    cout<<G;
    cout<<endl;
    
    cout<<endl<<"cin statement test: "<<endl;
    Month H;
    cin>>H;
    cout<<H;
    
    
    
    
    
    
    return 0;
}