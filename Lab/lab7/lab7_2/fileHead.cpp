#include<iostream>
#include<fstream>
using namespace std;

/*Write a program that asks the user for the name of a file. 

The program should display the first 10 lines of the file on the screen (the “head” of the file).

If the file has fewer than 10 lines the entire file should be displayed, 
(with a message indicating the entire file has been displayed.)*/

int main()
{
    cout<<"enter a file to view:  (for this example you should enter 'testFile') "<< endl;
    string userEntry , fileName;
    string fileContents = "";
    
    cin>>userEntry;
    cout<<"ok, opening."<<endl<<endl;
    fileName = userEntry + ".txt";
    ifstream handler;
    handler.open(fileName);
    int count  = 0;
    while(handler.peek()!= EOF && count < 10)
    {
        string temp; 
        std::getline(handler,temp);
        temp += "\n";
       fileContents += temp;
       count++;
    }
  
    cout<<endl<<fileContents<<endl;
    
    handler.close();
    
    return 0;
}