#include <iostream>
#include <fstream>
using namespace std;

/*
Write a program that reads and prints a joke and its punch line from two different files.




The main function of your program should open the two files and then call two functions, passing each one the file it needs.
The first function should read and display each line in the file it is passed (the joke file). 
The second function should display only the last line of the file it is passed (the punch line file). 
It should find this line by seeking to the end of the file and then backing up to the beginning of the last line. 
Data to test your program can be found in the joke.txt and punchline.txt files.
*/

int main()
{
    string fileName1 = "joke.txt";
    string fileName2 = "punchline.txt";
    string fileContents1 = "";
    string fileContents2 = "";
    
    ifstream handler;
    handler.open(fileName1);
    getline(handler, fileContents1);
    cout<<"Joke: "<<fileContents1<<endl;
    handler.close();
    
    
    handler.open(fileName2);
    while(handler.peek()!= EOF)
    {
        string temp; 
        std::getline(handler,temp);
        temp += "\n";
       fileContents2 = temp;
    }
    handler.close()
    
    cout<<"Punchline: " << fileContents2;
    
    
    
    
    
    return 0;
}