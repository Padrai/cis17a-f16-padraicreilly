#include <iostream>
#include "Transaction.h"

using namespace std;

//BIG 3:
//default constructor
Transaction::Transaction()
{   
    amount = 0;
    type = "";
    reoccurring = false;
    month = 0;
    day = 0;
    year = 0;
    
}

//overloaded constructor
Transaction::Transaction(double amount, string type,bool reoccurring,
                           int month,int day,int year)
{
    this -> amount = amount;
    this -> type = type;
    this -> reoccurring= reoccurring;
    this -> month= month;
    this -> day= day;
    this -> year= year;
    
}

//default destructor
Transaction::~Transaction(){}
    
//Custom copy COnstructor: Needs to create other examples of a reoccurring expense/income without 
//                   creating a spawning number of them. if it is a reoccurring expense, the one 
//                   initialized should be set to reoccurring = false;
Transaction::Transaction(const Transaction& right)
{
    
    reoccurring = right.reoccurring;
    
    amount = right.amount ;
    type = right.type;
    month = right.month;
    day = right.day;
    year = right.year;
}


//assignment operator overloads
 const Transaction& Transaction::operator=(const Transaction &orig)
 {
    this->amount = orig.amount;
    this ->type = orig.type;
    this ->reoccurring=orig.reoccurring ;
    this -> month = orig.month;
    this -> day = orig.day;
    this -> year = orig.year;
    
    return *this;
    
}

//Getters and Setters
//Getters
double Transaction::getAmount(){return amount;}
string Transaction::getType(){return type;}
bool Transaction::getReoccurring(){return reoccurring;}
int Transaction::getMonth(){return month;}
int Transaction::getDay(){return day;}
int Transaction::getYear(){return year;}
//Setters
void Transaction::setAmount(double amount){this->amount = amount;}
void Transaction::setType(string type){this->type = type;}
void Transaction::setReoccurring(bool reoccurring){this-> reoccurring= reoccurring;}
void Transaction::setMonth(int month){this-> month = month;}
void Transaction::setDay(int day){this->day = day;}
void Transaction::setYear(int year){this->year = year;}

//Printing
void Transaction::printMe()
{
    cout<<"Amount: "<<amount<<"\t Type: "<< type<<"\t Reoccurring: ";
    if(reoccurring == true){cout<<"Yes";}
    else{cout<<"No";}
    cout<<"\t Date: "<<month<<"/"<<day<<"/"<<year<<endl;
    
}
