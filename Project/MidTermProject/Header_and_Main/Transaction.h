#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <string>

using namespace std;

class Transaction
{
    private:
        double amount;
        string type;
        bool reoccurring;
        int month;
        int day;
        int year;
    public:
//BIG 3:
    //default constructor
        Transaction();
    //overloaded constructor
        Transaction(double amount, string type,bool reoccurring,
                    int month,int day,int year);
    //default destructor
        ~Transaction();
    
    //Custom copy COnstructor: Needs to create other examples of a reoccurring expense
    
    //income without creating a spawning number of them. if it is a reoccurring expense,
    //the one initialized should be set to reoccurring = false;
        Transaction(const Transaction&);


//operator overloads
    //assignment operator
        const Transaction& operator=(const Transaction&);
//getters and setters
        //setters
        void setAmount(double);
        void setType(string);
        void setReoccurring(bool);
        
        void setMonth(int);
        void setDay(int);
        void setYear(int);
        
        //getters
        double getAmount();
        string getType();
        bool getReoccurring();
        int getMonth();
        int getDay();
        int getYear();
        
        //printing
        void printMe();
        
        string transactionText();
        
};

#endif