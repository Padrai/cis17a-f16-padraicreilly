#include "User.h"
#include "Transaction.h"

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;
 //Big 3
    //default constructor
    User::User()
    {  
        name = "";
        password = "default";
        balance = 0.0;
        vector <Transaction> transactionList;
    }
    //destructor(delete transactionList)
        User::~User(){}
    //copy constructor (does vector have an overloaded assignment operator already?)
       User::User(const User& right)
       {
            this ->name =right.name ;
            this ->password =right.password ;
            this ->balance =right.balance;
            for (int i = 0; i< right.transactionList.size(); i++){this -> transactionList.push_back(right.transactionList.at(i));}
           
       }
    //overloaded constructor (initialize all variables)
        User::User(string name, string password, double balance, vector<Transaction> transactionList1)
        {
            this ->name = name;
            this ->password = password;
            this -> balance= balance;
            this -> transactionList.reserve(transactionList1.size());
            for (int i = 0; i< transactionList1.size(); i++){this->transactionList.at(i) = transactionList.at(i);}
        }
    //getters/setters
        //getters
       string User::getName(){return this->name;}
        string User::getPassword(){return this->password;}
        double User::getBalance(){return this->balance;}
        Transaction User::getTransaction(int index)
        {
            return transactionList.at(index);
        }
        
        //settters
        void User::setName(string name){this ->name = name;}
        void User::setPassword(string pw){this ->password = pw;}
        void User::updateBalance(double bal){this ->balance = bal;}
        void User::addTransaction(Transaction currentTransaction){transactionList.push_back(currentTransaction);}
        
        //printMe
        void User::printFullUser(){}
        void User::printUserAccount()
        {
            for(int i = 0; i < transactionList.size(); i++)
            {
                cout<< i + 1 <<": ";
                transactionList.at(i).printMe();
            }
            
        } 

void User::saveTransactions(){
    string filecontents = "";
    string filename = name + ".txt";
    ofstream handler;
    handler.open( filename.c_str() );
    
    for(int i=0 ; i<transactionList.size(); i++)
    {   handler << transactionList[i].getAmount()<<"\n";
     
        handler << transactionList[i].getType()<<"\n";
        
        handler << transactionList[i].getReoccurring()<<"\n";
        
        handler << transactionList[i].getMonth()<<"\n";
        
        handler << transactionList[i].getDay()<<"\n";
        
        handler << transactionList[i].getYear()<<"\n";
        
    }
    
    handler.close();
    
}

void User::loadTransactions()    //foor loop push_back into vthis->ector}
{  
    string fileName = "";
    fileName = name + ".txt";
    ifstream handler;
    handler.open(fileName.c_str());
    if(!handler){cout<<"No records found for this user."<<endl; }
    else{
        while(1)
        {
            
            //declare transction
           
            Transaction T;
            //declare variables
            double tempAm;
            string tempTy;
            bool tempRe;
            int tempMo, tempDa, tempYe;
            //declare strings and fill
            string stempAm,stempTy, stempRe, stempMo, stempDa, stempYe;
            
            handler>>stempAm;
            handler>>stempTy;
            handler>>stempRe;
            handler>>stempMo;
            handler>>stempDa;
            handler>>stempYe;
            //static cast
            tempAm = atof(stempAm.c_str());
            tempTy = stempTy;
            tempRe = atoi(stempRe.c_str());
            tempMo = atoi(stempMo.c_str());
            tempDa = atoi(stempDa.c_str());
            tempYe = atoi(stempYe.c_str());
        
            
            T.setAmount(tempAm);
            T.setType(tempTy);
            T.setReoccurring(tempRe);
            T.setMonth(tempMo);
            T.setDay(tempDa);
            T.setYear(tempYe);
            if(handler.eof()){break;}
            transactionList.push_back(T);
           
            
            
        }
        
        
        

    }
    handler.close();
}


double User::calcBalance()
{   double sum = 0;
    for(int i = 0; i< transactionList.size();i++) {sum += transactionList.at(i).getAmount();}
    return sum;
}

int User::TransactionListSize(){return  this->transactionList.size();}

void User::clearTransactions(){this -> transactionList.clear(); }
