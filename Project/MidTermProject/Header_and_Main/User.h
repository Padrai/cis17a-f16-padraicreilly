#ifndef USER_H
#define USER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "Transaction.h"


class User
{
    private:
        string name;
        string password;
        double balance;
        vector<Transaction> transactionList;
    public:
    //Big 3
        
        //constructor(must ask for username and password, and set balance to 0)
        User();
        //destructor(delete transactionList)
        ~User();
        //copy constructor (does vector have an overloaded assignment operator already?)
        User(const User&);
        //overloaded constructor (initialize all variables)
        User(string, string, double, vector<Transaction>);
    
    //getters/setters
        //getters
        string getName();
        string getPassword();
        double getBalance();
        Transaction getTransaction(int);
        
        //setters
        void setName(string);
        void setPassword(string);
        void updateBalance(double);
        void addTransaction(Transaction);

        //printMe
        void printFullUser();
        void printUserAccount();
        
        // filestuff
        void saveTransactions();
        void loadTransactions();
        
        double calcBalance();
        int TransactionListSize();
        
        void clearTransactions();
        
        
        
};  



#endif