#include "User.cpp"
#include "Transaction.cpp"
#include <iostream>
#include <vector>
#include <fstream>

void loadUsers(vector<User> &userList);
void addUser(vector<User> &userList);
bool doesUserExist(string searchTerm, vector<User> &userList);
int findUser(string searchTerm, vector<User> &userList);
void saveUserList(vector <User> &userList);
void createNewTransaction(User &currentUser);
void clear();
bool isDateCorrect(int, int);
void  filterTransactions(User );
vector <Transaction> RE_OC_FILT( User );
vector <Transaction> AMOUNT_FILT( User, double , double);
vector <Transaction> TYPE_FILT( User, string );

using namespace std;

int main()
{   //loading old users info
    vector<User> userList; 
    loadUsers(userList); 
    char optionInput;
    bool quit=false;
    //beginning menu loops  
    do
    {
        cout<<endl<<"Want to create a new user account (n) or log in to an existing user account (l) or quit(q)? "<<endl;
        cin>>optionInput;
        cout<<endl;
        if(optionInput== 'n')
        {
            cout<<"Adding user. "<<endl;
            addUser(userList);
        }
        
        else if(optionInput == 'l')
        {
        
            clear();
            cout<<"enter a username: "<<endl;
            string desiredUser;
            cin>>desiredUser;
           
            if(doesUserExist(desiredUser,userList) == true)
            {   int index = findUser(desiredUser, userList);
                int attempts = 0;
                bool loggedIn = false;
                while (attempts < 3 && !loggedIn)
                {
                    
                    cout<<"Enter the password: attempt "<<attempts + 1 <<" of 3: "<<endl;
                    string tempPassword;
                    cin>> tempPassword;
                    if(tempPassword == userList.at(index).getPassword())
                    {
                        clear();
                        cout<<"Logged in"<<endl;
                        loggedIn = true;
                    }
                    else{cout<<"Nope!"<<endl; attempts++;}
                
                    
                }
                
                if(loggedIn)
                {
                    userList.at(index).loadTransactions();
                    userList.at(index).printUserAccount();
                    cout<<endl;
                }
                while(loggedIn)
                {
                    cout<<"What would the user like to do? "<<endl;
                    do
                    {
                        cout<<endl<<"Add a transaction (a), view your transactions(v), update and \n"
                        <<"view your balance(b), log out (l), filter your files to view (f)\n "<<
                        "or log out and quit (q)"<<endl;
                        cin>>optionInput;
                        cout<<endl;
                        if (optionInput =='a'){createNewTransaction(userList.at(index)); }
                        else if (optionInput == 'f'){filterTransactions(userList.at(index));/*call a function to engage the fiter menu*/}
                        else if (optionInput == 'b'){userList[index].updateBalance(userList[index].calcBalance()); cout<<"Balance: "<<userList[index].getBalance()<<endl;}
                        else if (optionInput =='v'){cout<<endl<<"Viewing: "<<endl; userList.at(index).printUserAccount();}
                        else if (optionInput =='l'){loggedIn = false; userList.at(index).saveTransactions(); userList.at(index).clearTransactions();}
                        
                        else if (optionInput == 'q'){loggedIn = false; quit = true; userList.at(index).saveTransactions(); userList.at(index).clearTransactions();}
                    } while(optionInput != 'l' && optionInput != 'q' );
                }
            }
            else{cout<<"Username not found."<<endl;}
        }
        else if(optionInput == 'q'){quit=true;}
        else{cout<<"Invalid Input - Try Again"<<endl;}
    }while( quit == false );
    cout<<"Goodbye!"<<endl; 
    saveUserList(userList);
    return 0;
}

void loadUsers(vector<User> &userList){

    ifstream myfile;
    myfile.open ("userList.txt");
    while(myfile.peek() !=  EOF)
        {   
            User A;
            string tempName = "";
            string tempPassword = "";
            myfile>>tempName;
            myfile>>tempPassword;
            A.setName(tempName);
            A.setPassword(tempPassword);
            userList.push_back(A);
        }
    myfile.close();
}

void addUser(vector<User> &userList)
{
    User A;
    cout<<"Enter a Username (alphanumeric only, no spaces) for the account"<<endl;
    string tempName;
    string tempPassword;
    double tempBalance;
    cin>> tempName;
    cout<<"Enter a password"<<endl;
    cin>>tempPassword;
    A.setName(tempName);
    A.setPassword(tempPassword);
    userList.push_back(A);
}

bool doesUserExist(string searchTerm, vector<User> &userList)
{ 
    for (int i = 0; i< userList.size();i++)
    {
        if(userList.at(i).getName() == searchTerm){return true;}
    }
    return false;
}

int findUser(string searchTerm, vector<User> &userList)
{  
    for (int i = 0; i< userList.size();i++)
    {
        if(userList.at(i).getName() == searchTerm){return i;}
    }
}

void saveUserList(vector <User> &userList)
{
    fstream saveFile;
    saveFile.open("userList.txt", ios::out|ios::trunc);
    for (int i = 0; i<userList.size(); i++)
    {
        saveFile<<userList.at(i).getName()<<endl;
        saveFile<<userList.at(i).getPassword()<<endl;
    }
    saveFile.close();
    
}

void createNewTransaction(User &currentUser)
{
    double tempAmount;
    cout<<"Enter the amount"<<endl<<"$";
    cin>>tempAmount;
    
    string tempType;
    cout<<"Enter the type: "<<endl;
    cin>>tempType;
    
    int tempReoccurring;
    
    cout<<"Is it reocurring? one (1) for yes, zero (0) for no: "<<endl;
    while (cin>>tempReoccurring)
    {
        if (tempReoccurring != 1 && tempReoccurring != 0) { cout<<"Try again"<<endl;continue; }
        else{break;}
        
    }
    int tempMonth;
    int tempDay;
    for(;;)
    {
        cout<<"Enter the Month (1-12)"<<endl;
        cin>>tempMonth;
        cout<<"Enter the day (1-31)"<<endl;
        cin>>tempDay;
        if (isDateCorrect(tempMonth,tempDay) == true){break;}
    }
    
    int tempYear;
    cout<<"Enter the year (0000)"<<endl;
    cin>>tempYear;
    
    bool tempReoccurring2 = static_cast<bool>(tempReoccurring);
    
    Transaction C(tempAmount,tempType,tempReoccurring2,tempMonth,tempDay,tempYear);
    currentUser.addTransaction(C);
    C.printMe();
}

void clear()
{
    for(int i=0;i<100;i++){cout<<endl;}
}    

bool isDateCorrect(int month, int day)
{ 
    vector<int> numDays(13);
    numDays[0]= -1;
   numDays[1]= 31; 
   numDays[2]=28; 
   numDays[3]=31; 
   numDays[4] = 30;
   numDays[5]=31; numDays[6]=30; numDays[7]=31;
   numDays[8]= 31; numDays[9]=30; numDays[10]=31; numDays[11]=30; numDays[12]=31;
    if (month < 1 || month > 12){cout<<"Date invalid."<<endl; return false;}
    else if (day > numDays[month] || day < 1){cout<<"Date invalid."<<endl; return false;}
    else{return true;}
} 


void  filterTransactions(User current)
{
    vector <Transaction> result(0);
    char userC;
    while(1)
    {
        cout<<"would you like to filter by reocurring(r) , by a max and min amount(m), or by type (t)?"<<endl;
        cin>>userC;
        if(userC == 'r')
        {
        result = RE_OC_FILT(current); break;
        }
        else if(userC == 'm')
        {
            double min , max;
            while(1)
            {
                cout<<"enter min amount"<<endl;
                cin >>min;
                cout<<"enter max: "<<endl;
                cin>>max;
                if (min<max){cout<<endl;break;}
                else {cout<<"Limits incorrect"<<endl;}
            }
        result = AMOUNT_FILT(current, min, max); break;
        }
         else if(userC == 'm')
         {
             
         }
         else if(userC == 't')
         {
             string desiredType;
             cout<<"enter desired type: "<<endl;
             cin>>desiredType;
             cout<<endl;
             result = TYPE_FILT(current, desiredType);
             break;
         }
        else{cout<<"wrong answer!"<<endl; continue;}
    }
    cout<<"Filtered list:"<<endl;
    for (int i = 0; i < result.size(); i++)
    {
        result[i].printMe();
    }
    cout<<endl;
}


vector <Transaction> RE_OC_FILT( User current)
{
    vector <Transaction> temp;
    for(int i = 0; i < current.TransactionListSize(); i ++)
    {
        if(current.getTransaction(i).getReoccurring() == true)
        {
            temp.push_back(current.getTransaction(i));
        }
    }
    return temp;
}

vector <Transaction> AMOUNT_FILT( User current, double limitAmount1, double limitAmount2)
{
    vector<Transaction> results;
    for (int i = 0; i < current.TransactionListSize(); i++)
    {
        if (current.getTransaction(i).getAmount() >= limitAmount1 && current.getTransaction(i).getAmount() <= limitAmount2  )
        {
            results.push_back(current.getTransaction(i));
        }
    }
    return results;
}

vector <Transaction> TYPE_FILT( User current, string desiredtype)
{
    vector <Transaction> results;
    for (int i = 0; i < current.TransactionListSize(); i++)
    {
        if (current.getTransaction(i).getType() == desiredtype){results.push_back(current.getTransaction(i));}
    }
    return results;
    
}