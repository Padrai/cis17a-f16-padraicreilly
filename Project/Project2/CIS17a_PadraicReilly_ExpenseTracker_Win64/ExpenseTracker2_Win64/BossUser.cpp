#include "BossUser.h"
#include "Casual.h"
#include "CorporateEmployee.h"
#include "CorporateExpense.h"
#include "User.h"
#include "Transaction.h"

//See BossUser.h for comments explaining the purpose of each of these functions

CorporateEmployee BossUser::createEmployee(){
    cout<<"Creating new employee."<<endl;
    CorporateEmployee temp;
    string tempName, tempPW1, tempPW2;
    do{
        cout<<"Enter a Username: alphanumeric and  '_' ) ";
        getline(cin, tempName);
        if(this->employeeExists(tempName)){cout<<"Sorry, that employee name already exists. \n\n\n";}
    }while(this->employeeExists(tempName));
    temp.setName(tempName);
    do {
        cout<<"Enter a password: ";
        getline(cin, tempPW1);
        cout<<"Confirm password: ";
        getline(cin, tempPW2);
        if(tempPW1 == tempPW2){
            cout<<"Password successfully set."<<endl; 
            temp.setPassword(tempPW1); 
        }
        else{cout<<"Mismatch, try again."<<endl;}
    }while(tempPW1 != tempPW2);
    return temp;
 }

void BossUser::addEmployee(CorporateEmployee temp){
     this->Roster.push_back(temp);
}

void BossUser::deleteEmployee(int index){Roster.erase(Roster.begin() + index);}

void BossUser::saveRoster(){
    ofstream handler;
    string fileName;
    fileName = "RosterOf" + this->getName() + ".txt";
    handler.open(fileName.c_str());   
    for(int i = 0; i < this->RosterSize(); i++){
        handler << Roster[i].getName()<<endl;
        handler << Roster[i].getPassword()<<endl;
    }
    handler.close();
}

void BossUser::saveAllEmployeeExpenses(){
    for (int i = 0; i< this->RosterSize(); i++){
        ofstream handler;
        Roster[i].saveExpenses();
    }

}

void BossUser::loadRoster(){
    ifstream handler;
    string fileName = "RosterOf" + this->getName() + ".txt";
    handler.open(fileName.c_str());
    
    if(!handler){cout<<"No employees found under this employer: "<<this->getName()<<endl; }
    else{
        //handler.seekg(0,ios::beg);
        while(1){
            string tempName, tempPW;
            handler>>tempName;
            handler>>tempPW;
            CorporateEmployee temp;
            temp.setName(tempName);
            temp.setPassword(tempPW);
            if(handler.eof()){break;}
            Roster.push_back(temp);
        }
    }
}

void BossUser::loadAllEmployeeExpenses(){
    for(int i = 0; i< this->RosterSize(); i++){
        Roster[i].loadExpenses();
    }
}

bool BossUser::employeeExists(string tempName){
    for(int i = 0; i < Roster.size(); i++){
        if(tempName == Roster[i].getName()){
            return true;
        }
    }
    return false;
}

int BossUser::findEmployeeIndex(string tempName){
    for (int i = 0; i < Roster.size(); i++){
        if(tempName == Roster[i].getName()){
            return i;
        }
    }
    cout<<"findEmployeeInded AttemptedTO INDEX INVALID LOCATION"<<endl;
    return -1;

}

void BossUser::viewFullEmployeeProfile(int index){
    cout<<"Name: "<<Roster[index].getName()<<endl;
    cout<<"Password: "<<Roster[index].getPassword()<<endl;
    Roster[index].calcAndUpdateBalance();
    cout<<"Balance: $"<<Roster[index].getBalance()<<endl;
    cout<<endl;
}

void BossUser::viewAllEmployeeProfiles(){
    
    cout << "All Profiles: " << endl;
    for (int i = 0; i < this->RosterSize(); i++){
        cout<<i+1<<". ";
        viewFullEmployeeProfile(i);
    }
    cout<<"End of profiles."<<endl<<endl; 
}

void BossUser::viewAllEmployeeExpenses(int index){
    cout <<endl<<"Name: "<<Roster[index].getName()<<endl;
    for (int j  = 0; j < Roster[index].expenseListSize(); j++){
        Roster[index].printTransaction(j);
    }
}

void BossUser::viewByType(int index, string typeSearchTerm){
    cout<<"Name: "<<Roster[index].getName();
    Roster[index].viewByType(typeSearchTerm);
}

void BossUser::viewBeforeDate(int index, int month, int day, int year){
    cout<<"Name: "<<Roster[index].getName();
    Roster[index].viewBeforeDate(month, day, year);
}

void BossUser::viewAfterDate(int index, int month, int day, int year){
     cout<<"Name: "<<Roster[index].getName();
    Roster[index].viewAfterDate(month, day, year);
}

void BossUser::viewBetweenDates(int index,  int firstMonth, int firstDay, int firstYear, int secondMonth, int secondDay, int secondYear){}

void BossUser::approveAllUsersAllTransactions(bool decision){
    for (int i = 0; i < this->RosterSize(); i++)
    {
        for(int j = 0; j< Roster[i].expenseListSize();  j++){
            Roster[i].BossSetApproved(j,decision);
        }
    }
}

void BossUser::approveAllUsersUnderAmount(double maxAmount, bool decision){
    for(int i = 0; i < Roster.size(); i++){
        for(int j = 0; j < Roster[i].expenseListSize(); j++){
            CorporateExpense tempExpense = Roster[i].getExpense(j);
            if(tempExpense.getAmount() <= maxAmount){
                Roster[i].BossSetApproved(j,decision);
            }
        }
    }
}

void BossUser::approveAllUsersByType(string desiredType, bool decision){
    for(int i = 0; i < Roster.size(); i++){
        for(int j = 0; j < Roster[i].expenseListSize(); j++){
            CorporateExpense tempExpense = Roster[i].getExpense(j);
            if(tempExpense.getType() == desiredType){
                Roster[i].BossSetApproved(j,decision);
            }
        }
    }
}

void BossUser::approveAllOneUserByName(string desiredName, bool decision){
    bool userExists = false;
    for(int i = 0; i < Roster.size(); i++){
        if(Roster[i].getName()==desiredName){  
            for(int j = 0 ; j <Roster[i].expenseListSize(); j++){
                Roster[i].BossSetApproved(j,decision);
                
            }
        }
    }
}

void BossUser::approveOneExpense(int employeeIndex, int accountIndex, bool decision){
    Roster[employeeIndex].BossSetApproved(accountIndex, decision);
}

int BossUser::RosterSize(){return this->Roster.size();}

CorporateEmployee BossUser::getEmployee(int i){
    return Roster[i];
}

void BossUser::replaceEmployee(CorporateEmployee right, int index){
    Roster[index] = right;
}

void BossUser::reimburseAllApproved(int employeeIndex){
    double sum = 0;
    CorporateEmployee tempEmployee = this->getEmployee(employeeIndex);
    for(int i = 0; i < tempEmployee.expenseListSize(); i++){
        
        CorporateExpense tempExpense = tempEmployee.getExpense(i);
        if(tempExpense.getApproved()){
            sum += tempExpense.getAmount();
        }
        
    }
    if (sum != 0 || sum != -0){

        CorporateExpense reimbursement;
        reimbursement.setAmount(sum * -1);
        reimbursement.setType("Reimbursement");
        reimbursement.setReviewed(true);
        reimbursement.setApproved(true);
        tempEmployee.addExpense(reimbursement);
        this->replaceEmployee(tempEmployee, employeeIndex);
        cout<<endl;
    }
    else{cout<<"\nAll expenses for "<<tempEmployee.getName()<<" are reimbursed already."<<endl;}
}



