
/* 
 * File:   BossUser.h
 * Author: patrick
 *
 * Created on December 1, 2016, 12:11 AM
 */

#ifndef BOSSUSER_H
#define BOSSUSER_H
//Custom Header files
#include "Transaction.h"
#include "User.h"
#include "Casual.h"
#include "CorporateExpense.h"
#include "CorporateEmployee.h"

using namespace std;
//BossUser class inheriting from User base class
class BossUser
    :   public User{
private:
    vector <CorporateEmployee>  Roster; 
    
public:
    //load functions are called as soon as program starts.
    void loadRoster();//uses this->getName() to find an appropriately titled RosterOf<NAME>.txt file
    void loadAllEmployeeExpenses();//uses the names now in the Roster to find apprpriately titled ExpensesOf<EMPLOYEE>.txt file
    
    //save functions are called at the very end of the program;
    void saveRoster();
    void saveAllEmployeeExpenses();
    
    
    //employee account viewing functions , hopefully they are clearly named.
    void viewFullEmployeeProfile(int index); //shows a specific employees profile
    void viewAllEmployeeProfiles(); //shows all employee profiles under this boss
    
    void viewAllEmployeeExpenses(int); //shows an accounting of a specific employee's expenses and reimbursements
    
    void viewByType(int index, string typeSearchTerm); //shows expenses that match the string passed to it
    void viewBeforeDate(int index, int month, int day, int year); //shows expenses that are before the date passed to it
    void viewAfterDate(int index, int month, int day, int year); //shows expenses that are after the date passed to it
    
    //shows expenses between the two dates passed to it, and accounts for if date 2 is actually before date 1
    void viewBetweenDates(int index,  int firstMonth, int firstDay, int firstYear, int secondMonth, int secondDay, int secondYear);  
    
    
    int findEmployeeIndex(string tempName); //returns the index value in this bosses Roster of where the employee can be found whose name matches the string passed to this function
    
    void approveAllUsersAllTransactions(bool decision); //mass approval/denial
    void approveAllUsersUnderAmount(double maxAmount, bool decision); //mass approval/denial for anything under the amount passed to this function
    void approveAllUsersByType(string desiredType, bool decision);    //mass approval/denial for any transactions whose type match the string passed to this function
    void approveAllOneUserByName(string desiredName, bool decision);   //mass approval/denial for any transactions under the Employee whose name matches the string passed to this function
    void approveOneExpense(int UserIndex, int AccountIndex, bool decision); //single user, single expense approval/denial
    
    CorporateEmployee createEmployee(); //collects user info
    CorporateEmployee getEmployee(int i); //returns CorporateEmployee at Roster[i]
    void addEmployee(CorporateEmployee temp); //pushes back the CorporateEmployee passed to it. I call it by going : Bosses[thisBossIndex].addEmployee(createEmployee());
    void deleteEmployee(int index);  //deletes the employee at the index value passed
    int RosterSize(); //makes for loops a lot easier
    bool employeeExists(string tempName); //finds whether that name exists in this bosses Roster.
    
    void replaceEmployee(CorporateEmployee, int); //Using getEmployee and replaceEmployee allows me to pull a corporate employee from behind the private: specification and replace it when I am done with it 
    
    void reimburseAllApproved(int employeeIndex); //Once a boss has viewed and approved transactions, he can call this with a single stroke and have anything he has viewed and approved be reimbursed
};


#endif /* BOSSUSER_H */

