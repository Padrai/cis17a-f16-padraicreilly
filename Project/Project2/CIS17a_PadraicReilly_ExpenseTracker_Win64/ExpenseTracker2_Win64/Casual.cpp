#include "Transaction.h"
#include "User.h"
#include "Casual.h"
#include <vector>
#include <cstdlib>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

//See Casual.h for comments explaining the purpose of each of these functions

CasualUser::CasualUser()
    :   User(){
    balance = 0;
    transactionList;
    
}

CasualUser::CasualUser(const CasualUser &right){
    string tempName = right.getName();
    this->setName(tempName);
    this->setPassword(right.getPassword());
    for (int i = 0; i < right.TransactionListSize(); i++){
        transactionList.push_back(right.getTransaction(i));
    }
    this->calcAndSetBalance();
}

CasualUser::~CasualUser(){}

void CasualUser::addTransaction(Transaction right){ this->transactionList.push_back(right); }

Transaction CasualUser::recordTransaction(){
    cout<<"New Transaction:\n";
    Transaction temp;
    bool tryAgain  = true;
   
    //amount
    do {cout << "Amount? ";
    string userInput;
    getline(cin, userInput);
    double tempAmount = 0;;
    tempAmount = atof(userInput.c_str());
    if(tempAmount > 0 || tempAmount < 0){tryAgain = false; temp.setAmount(tempAmount);}
    }while(tryAgain);
    tryAgain  = true;
    
    //type
    cout<<endl<<"Type? ";
    string userInput;
    getline(cin, userInput);
    temp.setType(userInput);
   
    //reoccurring
    do{
        cout<<endl<<"Reoccurring? (y/n) ";
        string userInput;
        getline(cin, userInput);
        if(userInput == "y"){temp.setReoccurring(true); tryAgain = false;}
        else if(userInput == "n"){temp.setReoccurring(false); tryAgain = false;}
    }while(tryAgain);
    tryAgain  = true;
    
    //month
    do{
        cout<<endl<<"Month? ";
        string userInput;
        getline(cin, userInput);
        int tempMonth = atoi(userInput.c_str());
        if(tempMonth > 0 && tempMonth <= 12)
        {
            temp.setMonth(tempMonth); 
            tryAgain = false;
        }
    }while(tryAgain);
    tryAgain  = true;
   
    //day
    do{
       cout<<endl<<"Day? ";
       string userInput;
       getline(cin, userInput);
       int tempDay = atoi(userInput.c_str());
       if(tempDay > 0 && tempDay <= 31){tryAgain = false; temp.setDay(tempDay);}
    }while(tryAgain);
    tryAgain  = true;
   
    //year
     do {cout<<endl<<"Year? (last 2 digits) ";
        string userInput;
        getline(cin, userInput);
        int tempYear = atoi(userInput.c_str());
        if(tempYear > 0 && tempYear < 100){
            tryAgain = false;
            temp.setYear(tempYear);
        }
    }while(tryAgain);

    return temp;
}

Transaction CasualUser::getTransaction(int i)const{return transactionList[i];}

void CasualUser::editTransaction(int i){
    Transaction temp = this->recordTransaction();
    transactionList[i] = temp;
}

void CasualUser::deleteTransaction(int i){
    cout<<"Deleting transaction at index "<< i <<"."<<endl;
    if( i < 0 || i > transactionList.size())
    {
        cout<<"Invalid deletion index"<<endl;
        return;
    }
    else
    {
        transactionList.erase(transactionList.begin() + i);
    }
}


void CasualUser::saveTransactions() const{
    ofstream saveFile;
    string fileName = "/home/patrick/cis17a-f16-padraicreilly/Project/Project2/ExpenseTracker2/";
    string tempName = this->getName();
    fileName = "TransactionsOf" + tempName + ".txt";
    saveFile.open(fileName.c_str());
    for (int i = 0; i < this->TransactionListSize(); i++ ){
        saveFile << this->transactionList[i].getAmount()<<endl;
        saveFile << this->transactionList[i].getType()<<endl;
        saveFile << this->transactionList[i].getReoccurring()<<endl;
        saveFile << this->transactionList[i].getMonth()<<endl;
        saveFile << this->transactionList[i].getDay()<<endl;
        saveFile << this->transactionList[i].getYear()<<endl;
    }
    saveFile.close();
    
}

void CasualUser::loadTransactions(){   
{  
    string fileName = "TransactionsOf" + this->getName() + ".txt";
    ifstream handler;
    handler.open(fileName.c_str());
    if(!handler){cout<<"No records found for this user."<<endl;  }
    else{
        while(1)
        {
            //declare transction
            Transaction T;
            //declare variables
            double tempAm;
            string tempTy;
            bool tempRe;
            int tempMo, tempDa, tempYe;
            //declare strings and fill
            string stempAm,stempTy, stempRe, stempMo, stempDa, stempYe;
            
            getline(handler,stempAm);
            getline(handler,stempTy);
            getline(handler,stempRe);
            getline(handler,stempMo);
            getline(handler,stempDa);
            getline(handler,stempYe);
            //static cast
            tempAm = atof(stempAm.c_str());
            tempTy = stempTy;
            tempRe = atoi(stempRe.c_str());
            tempMo = atoi(stempMo.c_str());
            tempDa = atoi(stempDa.c_str());
            tempYe = atoi(stempYe.c_str());
        
            
            T.setAmount(tempAm);
            T.setType(tempTy);
            T.setReoccurring(tempRe);
            T.setMonth(tempMo);
            T.setDay(tempDa);
            T.setYear(tempYe);
            if(handler.eof()){break;}
            this->addTransaction(T);  
        }
    }
    handler.close();
}
    
    
}

void CasualUser::calcAndSetBalance(){
    double tempBal = 0;
    for (int i = 0; i < transactionList.size(); i++){
        tempBal += transactionList[i].getAmount();
    }
    balance = tempBal;
}

void CasualUser::viewAllTransactions()const{
    cout<<"\nAll Transactions: "<< endl;
    for(int i = 0; i < transactionList.size(); i++){
        cout<<i + 1 <<".  ";
        transactionList[i].printMe();
    }
    cout<<endl<<"BALANCE : " <<balance<<endl;
    cout<<endl;
}

int CasualUser::TransactionListSize()const{
    if(!transactionList.empty()){
    return transactionList.size();
    }else{
        return 0;
    }
}
