/* 
 * File:   Casual.h
 * Author: patrick
 *
 * Created on November 28, 2016, 7:37 PM
 */

#ifndef CASUAL_H
#define CASUAL_H
#include <vector>
#include <fstream>
#include "Transaction.h"
//CasualUser class inheriting from base User class
class CasualUser
    :   public User{
        private:
            double balance;
            vector<Transaction> transactionList;
        public:
            CasualUser();
            CasualUser(const CasualUser&);
            ~CasualUser();
            
            //Transaction list management 
            void addTransaction(Transaction);
            Transaction getTransaction(int)const;
            Transaction recordTransaction();
            void editTransaction(int);
            void deleteTransaction(int);
            void saveTransactions()const;
            void loadTransactions();
            void viewAllTransactions()const ;
            
            //makes for loops a lot easier
            int TransactionListSize() const;
            //updates balance
            void calcAndSetBalance();
            
            
            
};
#endif /* CASUAL_H */

