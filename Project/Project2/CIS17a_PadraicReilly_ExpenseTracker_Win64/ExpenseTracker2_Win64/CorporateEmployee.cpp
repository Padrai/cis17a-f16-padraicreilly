
#include "CorporateEmployee.h"
#include "Transaction.h"
#include "User.h"
#include <cstdlib>
#include <stdlib.h>

using namespace std;

//See CorporateEmployee.h for explanations of the purpose each of these functions

CorporateEmployee::CorporateEmployee()
    :   User(){
    balance = 0;
    expenseList.reserve(20);
}

CorporateEmployee::~CorporateEmployee(){}

CorporateEmployee::CorporateEmployee(const CorporateEmployee& right){
    this->setName(right.getName());
    this->setPassword(right.getPassword());
    for (int i = 0; i < right.expenseListSize(); i++){
        expenseList.push_back(right.getExpense(i));
    }
    this->calcAndUpdateBalance();
}

void CorporateEmployee::saveExpenses(){
    ofstream saveFile;
    string tempName = this->getName();
    string fileName = "ExpensesOf" + tempName + ".txt";
    saveFile.open(fileName.c_str());
    for (int i = 0; i < this->expenseListSize(); i++ ){
        saveFile << this->expenseList[i].getAmount()<<endl;
        saveFile << this->expenseList[i].getType()<<endl;
        saveFile << this->expenseList[i].getReoccurring()<<endl;
        saveFile << this->expenseList[i].getMonth()<<endl;
        saveFile << this->expenseList[i].getDay()<<endl;
        saveFile << this->expenseList[i].getYear()<<endl;
        saveFile << this->expenseList[i].getReviewed()<<endl;
        saveFile << this->expenseList[i].getApproved()<<endl;
    }
    saveFile.close();
}

void CorporateEmployee::loadExpenses(){
     string fileName = "ExpensesOf" + this->getName() + ".txt";
    ifstream handler;
    handler.open(fileName.c_str());
    if(!handler){cout<<"No records found for this user: "<<this->getName()<<endl; }
    else{
        while(1)
        {
            //declare expense
            CorporateExpense T;
            //declare variables
            double tempAm;
            string tempTy;
            bool tempReocc,tempReviewed, tempAppr;
            int tempMo, tempDa, tempYe;
            //declare strings and fill
            string stempAm,stempTy, stempMo, stempDa, stempYe, stempReocc,stempReviewed, stempAppr;
            
            getline(handler,stempAm);
            getline(handler,stempTy);
            getline(handler,stempReocc);
            getline(handler,stempMo);
            getline(handler,stempDa);
            getline(handler,stempYe);
            getline(handler,stempReviewed);
            getline(handler,stempAppr);
            
            
            //static cast
            tempAm = atof(stempAm.c_str());
            tempTy = stempTy;
            tempReocc = atoi(stempReocc.c_str());
            tempMo = atoi(stempMo.c_str());
            tempDa = atoi(stempDa.c_str());
            tempYe = atoi(stempYe.c_str());
            tempReviewed = atoi(stempReviewed.c_str());
            tempAppr = atoi(stempAppr.c_str());
        
            
            T.setAmount(tempAm);
            T.setType(tempTy);
            T.setReoccurring(tempReocc);
            T.setMonth(tempMo);
            T.setDay(tempDa);
            T.setYear(tempYe);
            T.setReviewed(tempReviewed);
            T.setApproved(tempAppr);
            
            if(handler.eof()){break;}
            this->addExpense(T);   
        }
    }
    handler.close();
}

void CorporateEmployee::viewAllExpenses(){
    cout << "All of " <<this->getName()<<"'s expenses: "<<endl;
    for (int i = 0; i < this->expenseListSize(); i++){
        cout<< i + 1 <<". ";
        expenseList[i].printMe();
    }
    cout<<endl;
}

void CorporateEmployee::addExpense(CorporateExpense right){this->expenseList.push_back(right);}

CorporateExpense CorporateEmployee::recordExpense(){
    cout<<"New Transaction:\n";
    CorporateExpense temp;
    bool tryAgain  = true;
   
    //amount
    do {cout << "Amount? ";
    string userInput;
    getline(cin, userInput);
    double tempAmount = 0;;
    tempAmount = atof(userInput.c_str());
    if(tempAmount > 0 || tempAmount < 0){tryAgain = false; temp.setAmount(tempAmount);}
    }while(tryAgain);
    tryAgain  = true;
    
    //type
    cout<<endl<<"Type? ";
    string userInput;
    getline(cin, userInput);
    temp.setType(userInput);
   
    //reoccurring
    do{
        cout<<endl<<"Reoccurring? (y/n) ";
        string userInput;
        getline(cin, userInput);
        if(userInput == "y"){temp.setReoccurring(true); tryAgain = false;}
        else if(userInput == "n"){temp.setReoccurring(false); tryAgain = false;}
    }while(tryAgain);
    tryAgain  = true;
    
    //month
    do{
        cout<<endl<<"Month? ";
        string userInput;
        getline(cin, userInput);
        int tempMonth = atoi(userInput.c_str());
        if(tempMonth > 0 && tempMonth <= 12)
        {
            temp.setMonth(tempMonth); 
            tryAgain = false;
        }
    }while(tryAgain);
    tryAgain  = true;
   
    //day
    do{
       cout<<endl<<"Day? ";
       string userInput;
       getline(cin, userInput);
       int tempDay = atoi(userInput.c_str());
       if(tempDay > 0 && tempDay <= 31){tryAgain = false; temp.setDay(tempDay);}
    }while(tryAgain);
    tryAgain  = true;
   
    //year
     do {cout<<endl<<"Year? (last 2 digits) ";
        string userInput;
        getline(cin, userInput);
        int tempYear = atoi(userInput.c_str());
        if(tempYear > 0 && tempYear < 100){
            tryAgain = false;
            temp.setYear(tempYear);
        }
    }while(tryAgain);

    return temp;
}

void CorporateEmployee::recordAccountInfo(){
    cout<<endl<<"Enter the name: ";
    string tempName, tempPW;
    getline(cin,tempName);
    this->setName(tempName);
   
    cout<<"Enter a password: "<<endl;
    getline(cin, tempPW);
    this->setPassword(tempPW);
}

CorporateExpense CorporateEmployee::getExpense(int i) const{
    if (i > this->expenseListSize()){
        cout<<"FAILURE"<<endl; 
        exit(0);
    }
    else {
        return expenseList[i];
    }
}

void CorporateEmployee::editExpense(int i){
    if(i < 0 || i >= this->expenseListSize()){
        cout<<"invalid index provided"<<endl; 
        return;
    }else{
        cout<<"Editing Expense at index "<<i<<": "<<endl;
        CorporateExpense temp  = this->recordExpense();
        expenseList[i] = temp;
    }
}

void CorporateEmployee::deleteExpense(int i){
    if(i < 0 || i >= this->expenseListSize() ){
        cout<<"invalid index provided"<<endl; 
        return;
    }else{
    expenseList.erase(expenseList.begin() + i);
    }
}

void CorporateEmployee::calcAndUpdateBalance(){
    double tempBal = 0;
    for (int i = 0; i < expenseList.size(); i++){
        tempBal += expenseList[i].getAmount();
    }
    balance = tempBal;
}

int CorporateEmployee::expenseListSize() const {return this->expenseList.size();}  

void CorporateEmployee::BossSetReviewed(int index){
    expenseList[index].setReviewed(true);
}

void CorporateEmployee::BossSetApproved(int index, bool bossDecision){
    BossSetReviewed(index);
    expenseList[index].setApproved(bossDecision);
}

void CorporateEmployee::printTransaction(int index){
    expenseList[index].printMe();
}

double CorporateEmployee::getBalance(){return balance;}



void CorporateEmployee::viewByType(string typeDesired){
    for(int j = 0; j < expenseList.size(); j++){
        if(expenseList[j].getType() == typeDesired){
            expenseList[j].printMe();
        } 
    }
}

void CorporateEmployee::viewUnderMax(double maximum){
    bool none  = true;
    for(int i = 0; i < expenseList.size(); i++){
        if(expenseList[i].getAmount() <= maximum){
            expenseList[i].printMe(); 
            none = false;
        }
    }
    if(none){cout<<"Nothing under that amount found."<<endl<<endl;}
}

void CorporateEmployee::viewOverMin(double minimum){
    bool none  = true;
    for(int i = 0; i < expenseList.size(); i++){
        if(expenseList[i].getAmount() >= minimum){
            expenseList[i].printMe(); 
            none = false;
        }
    }
    if(none){cout<<"Nothing over that amount found."<<endl<<endl;}
}

void CorporateEmployee::viewInRange(double minimum, double maximum){
    bool none  = true;
    if(minimum > maximum){
        cout<<"Maximum is less than minimum, switching values"<<endl;
        double temp = maximum; 
        maximum = minimum; 
        minimum = temp;
    }
    for(int i = 0; i < expenseList.size(); i++){
        if(expenseList[i].getAmount() >= minimum && expenseList[i].getAmount() <=maximum){
            expenseList[i].printMe(); 
            none = false;
        }
    }
    if(none){cout<<"\nNothing between those amounts found.\n\n";}
}

void CorporateEmployee::viewApproved(){
    bool none = true;
    cout<<"\n";
    for(int i = 0; i < expenseList.size(); i++){
        if(expenseList[i].getApproved() && expenseList[i].getReviewed()){
            expenseList[i].printMe();
            none = false;
        }
    }
    if(none){cout<<"\nNo approved transactions.";}
    cout<<"\n\n";
}

void CorporateEmployee::viewDenied(){
    bool none = true;
    cout<<"\n";
    for(int i = 0; i < expenseList.size(); i++){
        if(!expenseList[i].getApproved() && expenseList[i].getReviewed()){
            expenseList[i].printMe();
            none = false;
        }
    }
    if(none){cout<<"\nNo denied transactions.";}
    cout<<"\n\n";
}

void CorporateEmployee::viewPending(){
    bool none = true;
    cout<<"\n";
    for(int i = 0; i < expenseList.size(); i++){
        if(!expenseList[i].getReviewed()){
            expenseList[i].printMe();
            none = false;
        }
    }
    if(none){cout<<"\nNo transactions currently pending. "<<endl;}
    cout<<"\n\n";

}

bool CorporateEmployee::firstDateIsFirst(int firMonth, int firDay, int firYear, int secMonth, int secDay, int secYear){
    if(firYear<secYear){return true;}
    if(firYear == secYear && firMonth < secMonth){return true;}
    if(firYear == secYear && firMonth == secMonth && firDay < secDay){return true;}
    else{return false;}
}

void CorporateEmployee::viewAfterDate(int begMo, int begDay, int begYear){
    if(begMo > 12 || begMo < 1 || begDay> 31  ||begDay < 1 || begYear < 0 ||begYear > 99){cout<<"\nInvalid date supplied\n\n"; return;}
    bool none = true;
    for(int i = 0; i < expenseList.size(); i++){
        if(this->firstDateIsFirst(begMo, begDay, begYear,expenseList[i].getMonth(),expenseList[i].getDay(),expenseList[i].getYear())){
            expenseList[i].printMe();
            none = false;
        }
    }
    if(none){cout<<"\nNo transactions after this date.\n";}
}

void CorporateEmployee::viewBeforeDate(int endMo, int endDay, int endYear){
    if(endMo > 12 || endMo < 1 || endDay> 31  || endDay < 1 || endYear < 0 || endYear > 99){cout<<"\nInvalid date supplied\n\n"; return;}
    bool none = true;
    for(int i = 0; i < expenseList.size(); i++){
        if(this->firstDateIsFirst(expenseList[i].getMonth(),expenseList[i].getDay(),expenseList[i].getYear(), endMo, endDay, endYear)){
            expenseList[i].printMe();
            none = false;
        }
    }
    if(none){cout<<"\nNo transactions before this date.\n";}
}


void CorporateEmployee::viewBetweenDates(int begMo, int begDay, int begYear, int endMo, int endDay, int endYear){
    if(endMo > 12 || endMo < 1 || endDay> 31  || endDay < 1 || endYear < 0 || endYear > 99){cout<<"\nInvalid date supplied\n\n"; return;}
    if(begMo > 12 || begMo < 1 || begDay> 31  ||begDay < 1 || begYear < 0 ||begYear > 99){cout<<"\nInvalid date supplied\n\n"; return;}
    bool none = true;
    for(int i = 0; i < expenseList.size(); i++){
        if(this->firstDateIsFirst(begMo, begDay, begYear,expenseList[i].getMonth(),expenseList[i].getDay(),expenseList[i].getYear())
        &&
        this->firstDateIsFirst(expenseList[i].getMonth(),expenseList[i].getDay(),expenseList[i].getYear(), endMo, endDay, endYear)){
            expenseList[i].printMe();
            none = false;
        }
    }
    if(none){cout<<"\nNo transactions before this date.\n";}
}

