
#ifndef CORPORATEEMPLOYEE_H
#define CORPORATEEMPLOYEE_H
//custom classes
#include "Transaction.h"
#include "CorporateExpense.h"
#include "User.h"
//standard libraries
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
//for simplicity
using namespace std;

class CorporateEmployee
    : public User{
    
private:
    vector <CorporateExpense> expenseList;
    double balance;
    string supervisor;
public:
    //Big 3
    CorporateEmployee();
    CorporateEmployee(const CorporateEmployee&);
    ~CorporateEmployee();
    
    
    void loadExpenses();//called upon program start
    void saveExpenses();//called upon program end 
    
    //viewing expenses
    void viewAllExpenses();
    void viewByType(string);
        //view by amount
    void viewUnderMax(double);
    void viewOverMin(double);
    void viewInRange(double, double);
        //view by approval status
    void viewApproved();
    void viewDenied();
    void viewPending();
        //view by date
    void viewAfterDate(int, int, int);
    void viewBeforeDate(int, int, int);
    void viewBetweenDates(int, int,int, int,int, int );
    
    bool firstDateIsFirst(int, int, int, int, int, int);//determines order based on date
    
    //expense list management
    void addExpense(CorporateExpense);
    CorporateExpense recordExpense();
    CorporateExpense  getExpense(int) const;
    void editExpense(int);
    void deleteExpense(int);
    
    
    //functions for boss to set approval/denied status
    void BossSetReviewed(int);
    void BossSetApproved(int, bool);
    
    //prints a specific transaction at index i
    void printTransaction(int);
    
    
    void calcAndUpdateBalance(); //updates balance rather than setBalance()
    double getBalance();
    
    int expenseListSize() const; //makes for loops easier
    
    void recordAccountInfo();//makes account creation easy 
    
    
    
};

#endif /* CORPORATEEMPLOYEE_H */

