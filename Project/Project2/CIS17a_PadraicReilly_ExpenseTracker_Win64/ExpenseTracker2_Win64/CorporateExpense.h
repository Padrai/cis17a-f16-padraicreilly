

/* 
 * File:   CorporateExpense.h
 * Author: patrick
 * Created on November 26, 2016, 12:27 AM
 */

#ifndef CORPORATEEXPENSE_H
#define CORPORATEEXPENSE_H
#include "Transaction.h"
//CorporateExpense class inheriting from Transaction base class.
class CorporateExpense 
    :   public Transaction
 {
        
      private:
          bool reviewed; //has the boss made a decision?
          bool approved; //what was the bosses decision?
      public:
          //big 3 (default destructor works perfectly)
          CorporateExpense();
          CorporateExpense(const CorporateExpense&);
          //setters
          void setReviewed(bool);
          void setApproved(bool);
          //getters
          bool getReviewed() const;
          bool getApproved() const;  
          //printMe
          virtual void printMe();
};

#endif /* CORPORATEEXPENSE_H */