using namespace std;
#include "Transaction.h"

//see Transaction.h for explanations of these functions purposes.

Transaction::Transaction(){
        amount = 0; 
        type = "";
        reoccurring = false;
        month= 0;
        day = 0;
        year = 0;
}

Transaction::Transaction(double amount, string type,bool reoccurring,
                    int month,int day,int year)
{
    this->amount = amount;
    this->type = type;
    this->reoccurring = reoccurring;
    this->month = month;
    this->day = day;
    this->year = year;
}

Transaction::~Transaction(){}

//Setters
void Transaction::setAmount(double amount){this->amount = amount;}

void Transaction::setType(string type){this->type = type;}

void Transaction::setReoccurring(bool reoccurring){this->reoccurring = reoccurring;}

void Transaction::setMonth(int month){this-> month = month;}

void Transaction::setDay(int day){this-> day = day;}

void Transaction::setYear(int year){this->year = year;}

//Getters
double Transaction::getAmount()const{return amount;} 

string Transaction::getType()const{return type;}

bool Transaction::getReoccurring() const{return reoccurring;}

int Transaction::getMonth() const {return month;}

int Transaction::getDay() const {return day;}

int Transaction::getYear() const{return year;}

//print 1 transaction
void Transaction::printMe() const
{
    cout<< "Amount: " << amount << "\t" << "Type:  " << type << "\t\t" << "Reoccurring: ";
    if(reoccurring){cout<<"Yes";}
    else{cout<<"No";}
    cout << "\t\t" << "Date: " << month << "/" << day << "/" << year;
    cout << endl;
}
