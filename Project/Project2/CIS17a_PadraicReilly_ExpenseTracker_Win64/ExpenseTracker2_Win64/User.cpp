#include <iostream>
#include "User.h"
using namespace std;

//See User.h for explanations of these functions purposes

User::User(){
    name = "";
    password = "";
}
User::~User(){}
//getters
string User::getName() const { return name;}
string User::getPassword() const {return password;}

//setters 
void User::setName(string name){ this->name = name; }
void User::setPassword(string password){this->password = password;}