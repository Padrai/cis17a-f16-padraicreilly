
/* 
 * File:   User.h
 * Author: patrick
 *
 * Created on November 25, 2016, 11:00 PM
 */

#ifndef USER_H
#define USER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "Transaction.h"
using namespace std;

//User base class
class User
{
    private:
        string name;
        string password;
        
    public:
        //Big 3
        User();
        ~User();
        
        //getters
        string getName() const;
        string getPassword() const;
                
        //setters
        void setName(string);
        void setPassword(string);
};  


#endif /* USER_H */

