
/* 
 * File:   BossUser.h
 * Author: patrick
 *
 * Created on December 1, 2016, 12:11 AM
 */

#ifndef BOSSUSER_H
#define BOSSUSER_H
//Custom Headers
#include "Transaction.h"
#include "User.h"
#include "Casual.h"
#include "CorporateExpense.h"
#include "CorporateEmployee.h"
//Standard Libraries
using namespace std;

class BossUser
    :   public User{
private:
    vector <CorporateEmployee>  Roster;     
public:
    void loadRoster();
    void loadAllEmployeeExpenses();
    
    void saveRoster();
    void saveAllEmployeeExpenses();
    
    void viewFullEmployeeProfile(int index);
    void viewAllEmployeeProfiles();
    
    void viewAllEmployeeExpenses(int);
    
    void viewByType(int index, string typeSearchTerm);
    void viewBeforeDate(int index, int month, int day, int year);
    void viewAfterDate(int index, int month, int day, int year);
    void viewBetweenDates(int index,  int firstMonth, int firstDay, int firstYear, int secondMonth, int secondDay, int secondYear);
    
    int findEmployeeIndex(string tempName);
    
    void viewAmountHighToLow(int index);
    void viewAmountLowToHigh(int index);
    void viewAmountOverAmount(int index);
    
    void approveAllUsersAllTransactions(bool decision);
    void approveAllUsersUnderAmount(double maxAmount, bool decision);
    void approveAllUsersByType(string desiredType, bool decision);
    void approveAllOneUserByName(string desiredName, bool decision);
    void approveOneExpense(int UserIndex, int AccountIndex, bool decision);
    
    CorporateEmployee createEmployee();
    CorporateEmployee getEmployee(int i);
    void addEmployee(CorporateEmployee temp);
    void deleteEmployee(int index);  
    int RosterSize();
    bool employeeExists(string tempName);
    
    void replaceEmployee(CorporateEmployee, int);
    
    
};


#endif /* BOSSUSER_H */

