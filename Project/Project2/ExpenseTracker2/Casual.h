/* 
 * File:   Casual.h
 * Author: patrick
 *
 * Created on November 28, 2016, 7:37 PM
 */

#ifndef CASUAL_H
#define CASUAL_H
#include <vector>
#include <fstream>
#include "Transaction.h"
class CasualUser
    :   public User{
        private:
            double balance;
            vector<Transaction> transactionList;
        public:
            CasualUser();
            CasualUser(const CasualUser&);
            ~CasualUser();
            void addTransaction(Transaction);
            Transaction getTransaction(int)const;
            Transaction recordTransaction();

            void editTransaction(int);
            void deleteTransaction(int);
            void clearTransactions();
            void saveTransactions()const;
            void loadTransactions();
            void viewAllTransactions()const ;
            void calcAndSetBalance();
            int TransactionListSize() const;
            
            
};
#endif /* CASUAL_H */

