
#ifndef CORPORATEEMPLOYEE_H
#define CORPORATEEMPLOYEE_H
//custom classes
#include "Transaction.h"
#include "CorporateExpense.h"
#include "User.h"
//standard libraries
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
//for simplicity
using namespace std;

class CorporateEmployee
    : public User{
    
private:
    vector <CorporateExpense> expenseList;
    double balance;
    string supervisor;
public:
    CorporateEmployee();
    CorporateEmployee(const CorporateEmployee&);
    ~CorporateEmployee();
    
    void loadExpenses();
    void saveExpenses();
    //view all
    void viewAllExpenses();
    //view by type
    void viewByType(string);
    //view by amount
    void viewUnderMax(double);
    void viewOverMin(double);
    void viewInRange(double, double);
    //view by approval status
    void viewApproved();
    void viewDenied();
    void viewPending();
    //view by date
    void viewAfterDate(int, int, int);
    void viewBeforeDate(int, int, int);
    void viewBetweenDates(int, int,int, int,int, int );
    
    bool firstDateIsFirst(int, int, int, int, int, int);
    
    
    void addExpense(CorporateExpense);
    CorporateExpense recordExpense();
    
    CorporateExpense  getExpense(int) const;
    void editExpense(int);
    void deleteExpense(int);
    
    //int findTransaction();
    
    
    void BossSetReviewed(int );
    void BossSetApproved(int, bool);
    
    void printTransaction(int);
    
    
    void calcAndUpdateBalance();
    int expenseListSize() const;
    
    void recordAccountInfo();
    
    double getBalance();
    
};

#endif /* CORPORATEEMPLOYEE_H */

