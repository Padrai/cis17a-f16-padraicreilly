#include "CorporateExpense.h"
#include "Transaction.h"
#include <iomanip>
#include <iostream>

CorporateExpense::CorporateExpense()
    :   Transaction()
{
    reviewed = false;
    approved = false;
}

CorporateExpense::CorporateExpense(const CorporateExpense& right){
    this->setAmount(right.getAmount());
    this->setType(right.getType());
    this->setReoccurring (right.getReoccurring());
    this-> setMonth(right.getMonth());
    this->setDay(right.getDay());
    this->setYear(right.getYear());
            
    reviewed = right.getReviewed();
    approved = right.getApproved();
}

void CorporateExpense::setReviewed(bool reviewed){this->reviewed = reviewed;}

void CorporateExpense::setApproved(bool approved){this->approved = approved;}

bool CorporateExpense::getReviewed() const{return reviewed;}

bool CorporateExpense::getApproved() const{return approved;}

void CorporateExpense::printMe()
{
    cout<< "Amount: " << this->getAmount() << "\t" << "Type: " << this->getType() << "\t\t" << "Reoccurring: ";
    
    if(this->getReoccurring()){cout<<"Yes";}
    else{cout<<"No ";}
    
    cout << "\t" << "Date: " << this->getMonth()<< "/" << this->getDay() << "/" << this->getYear();
    if(!reviewed){cout<<"\tReviewed: No";}
    else
    {   cout << "\tReviewed: Yes";
        cout << "\t " << "Approved: "; 
        if(approved){cout<<"Yes";}
        else {cout <<"No";}
    }
    cout << endl; 
    
}