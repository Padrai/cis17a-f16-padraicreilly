

/* 
 * File:   CorporateExpense.h
 * Author: patrick
 * Created on November 26, 2016, 12:27 AM
 */

#ifndef CORPORATEEXPENSE_H
#define CORPORATEEXPENSE_H
#include "Transaction.h"
class CorporateExpense 
    :   public Transaction
 {
        
      private:
          bool reviewed;
          bool approved;
      public:
          CorporateExpense();
          CorporateExpense(const CorporateExpense&);
          void setReviewed(bool);
          void setApproved(bool);
          bool getReviewed() const;
          bool getApproved() const;  
          void printMe();
};

#endif /* CORPORATEEXPENSE_H */