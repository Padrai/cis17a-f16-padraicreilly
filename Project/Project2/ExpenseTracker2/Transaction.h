

/* 
 * File:   Transaction.h
 * Author: patrick
 *
 * Created on November 25, 2016, 10:59 PM
 */

#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <string>
#include<iomanip>
#include<iostream>

using namespace std;

class Transaction
{
    private:
        double amount;
        string type;
        bool reoccurring;
        int month;
        int day;
        int year;
    public:
    //default constructor
        Transaction();
    //overloaded constructor
        Transaction(double amount, string type,bool reoccurring, int month,int day,int year);
    //default destructor
        ~Transaction();
    
//getters and setters
        //setters
        void setAmount(double);
        void setType(string);
        void setReoccurring(bool);
        void setMonth(int);
        void setDay(int);
        void setYear(int);
        
        //getters
        double getAmount() const;
        string getType() const;
        bool getReoccurring() const;
        int getMonth() const;
        int getDay() const;
        int getYear() const;
        
        //printing
        virtual void printMe() const;     
};

#endif

