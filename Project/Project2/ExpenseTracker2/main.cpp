/* 
 * File:   main.cpp
 * Author: patrick
 *
 * Created on November 25, 2016, 10:56 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include "Transaction.h"
#include "User.h"
#include "Casual.h"
#include "Transaction.h"
#include "CorporateExpense.h"
#include "CorporateEmployee.h"
#include "BossUser.h"

using namespace std;

void saveCasualUserList(vector <CasualUser> casualUserList){
    ofstream handler;
    string fileName = "Casuals.txt";
    handler.open(fileName.c_str());
    for(int i = 0; i < casualUserList.size(); i++){
        handler<<casualUserList[i].getName()<<endl;
        handler<<casualUserList[i].getPassword()<<endl;
    }
    handler.close();
}
void saveBossUserList(vector <BossUser>bossUserList){
    ofstream handler;
    string fileName = "Bosses.txt";
    handler.open(fileName.c_str());
    for(int i = 0; i < bossUserList.size(); i++){
        handler<<bossUserList[i].getName()<<endl;
        handler<<bossUserList[i].getPassword()<<endl;
    }
    handler.close();
    handler.clear();
    
    for (int i = 0; i < bossUserList.size(); i++){
        bossUserList[i].saveRoster();
        bossUserList[i].saveAllEmployeeExpenses();
    }
}

void loadBossUserList(vector <BossUser> &bossUserList){
    ifstream handler;
    string fileName = "Bosses.txt";
    handler.open(fileName.c_str());
    if(!handler){cout<<"Boss List not found."<<endl;}
    else{
        while(1){
            string tempName;
            string tempPW;
            getline(handler, tempName);
            getline(handler, tempPW);
            
            BossUser temp;
            temp.setName(tempName);
            temp.setPassword(tempPW);
            if(handler.eof()){break;}
            bossUserList.push_back(temp);
        }
    }
}
void loadCasualUserList(vector<CasualUser> &casualUserList){
    ifstream handler;
    string fileName = "Casuals.txt";
    handler.open(fileName.c_str());
    if(!handler){cout<<"Casuals List not found."<<endl;}
    else{
        while(1){
            string tempName;
            string tempPW;
            getline(handler, tempName);
            getline(handler, tempPW);
            
            CasualUser temp;
            temp.setName(tempName);
            temp.setPassword(tempPW);
            if(handler.eof()){break;}
            casualUserList.push_back(temp);
        }
    }
}

BossUser createBossUser(vector <BossUser> listToCompare){
    BossUser A;
    bool alreadyExists = false;
    
    cout<<"Creating new boss."<<endl;
    
    do{
        cout<<"Enter your name: ";
        string tempName;
        getline(cin,tempName);
        for(int i = 0; i < listToCompare.size(); i++){
            
            if(listToCompare[i].getName() == tempName){
                alreadyExists = true;
                cout<<"User Already exists. Try another name."<<endl<<endl;
                break;
            }
            else{alreadyExists = false;}
            
        }
        
        A.setName(tempName);
        
    }while(alreadyExists);
    
    string tempPW1, tempPW2;
    do{
        cout<<"Enter your new password: ";
        getline(cin, tempPW1);
        cout<<"Confirm password: ";
        getline(cin, tempPW2);
        A.setPassword(tempPW1);
        if(tempPW1 != tempPW2){cout<<"Password not confirmed."<<endl<<endl;}
    }while(tempPW1 != tempPW2 );
    
      return A;  
    }
bool foundBossUser(string searchName, const vector<BossUser> &Bosses){
    for(int i = 0; i < Bosses.size(); i++){
        if(Bosses[i].getName() == searchName){return true;}
    }
    return false;
}
int findBossUser(string searchName, const vector <BossUser> &Bosses){
    for(int i = 0; i<Bosses.size(); i++){
        if(Bosses[i].getName() == searchName){return i;}
    }
    return -1;
}

CasualUser createCasualUser(vector <CasualUser> listToCompare){
    cout<<"Creating Casual User."<<endl;
    CasualUser A;
    bool alreadyExists;
    string tempCasualUserName;
    do{
        //USer name business
        cout<<"Enter a Name: ";
        getline(cin, tempCasualUserName);
        
        
        for(int i =0 ; i< listToCompare.size(); i++){
            if(tempCasualUserName == listToCompare[i].getName()){
                alreadyExists = true;
                cout<<"User already exists, try something else."<<endl;
                break;
            }
            else{alreadyExists = false;}
        }
    }while(alreadyExists);
    A.setName(tempCasualUserName);
    //Password business
    bool notEqual = true;
    string tempPW1, tempPW2;
    do{
        cout<<"Enter a new password: ";
        getline(cin, tempPW1);
        cout<<"Confirm password:";
        getline(cin, tempPW2);
        if(tempPW1 == tempPW2){
            notEqual = false; 
            cout<<"Password confirmed"<<endl;
        }
        else{cout<<"Password not confirmed."<<endl;}
    }while(notEqual);
    A.setPassword(tempPW1);
    return A;
    
    
}
bool foundCasualUser(string searchName, const vector<CasualUser> &Casuals){
    for(int i = 0; i < Casuals.size(); i++){
        if(Casuals[i].getName() == searchName){return true;}
    }
    return false;
}
int findCasualUser(string searchName, const vector <CasualUser> &Casuals){
    for(int i = 0; i<Casuals.size(); i++){
        if(Casuals[i].getName() == searchName){return i;}
    }
    return -1;
}

bool firstDateisFirst(int firMonth, int firDay, int firYear, int secMonth, int secDay, int secYear){
    if(firYear<secYear){return true;}
    if(firYear == secYear && firMonth < secMonth){return true;}
    if(firYear == secYear && firMonth == secMonth && firDay < secDay){return true;}
    else{return false;}
}
bool firstDateisSecond(int firMonth, int firDay, int firYear, int secMonth, int secDay, int secYear){
    if(firYear>secYear){return true;}
    if(firYear == firMonth && firMonth > secMonth){return true;}
    if(firYear == firMonth && firMonth == secMonth && firDay > secDay){return true;}
    else{return false;}
}

int main() {
    //intialize containers
    vector <BossUser> Bosses; //for the Bosses
    Bosses.reserve(10);
    
    vector <CasualUser> Casuals; // for the Casuals
    Casuals.reserve(20);
    //load containers from respective files
    loadBossUserList(Bosses);
    loadCasualUserList(Casuals);
            
    for(int i = 0; i < Casuals.size(); i++){
        Casuals[i].loadTransactions();
    }
    for (int i = 0; i < Bosses.size(); i++){
        Bosses[i].loadRoster();
        Bosses[i].loadAllEmployeeExpenses();
    }
            
    // menu content
    bool quit = false;
    //looping structure
    while(!quit){
        //OUTERMOST LAYERlAYER
        string userTypeChoice;
        cout<<"Which type of User are you?  'q' to quit."<<endl; 
        cout<<"Casual : 'c' \t Professional : 'p'"<<endl;
        getline(cin, userTypeChoice);
        if(userTypeChoice == "q"){quit = true;}
        else if(userTypeChoice == "c"){
            //CASUAL USER LAYER
            
            cout<<"Public Casual User Mode Options: "<<endl;
            cout<<"Quit: 'q' \t Add a new Casual User: 'a' \t Login to Casual User account: 'l' "<<endl<<endl;
            string casualUserChoice;
            getline(cin, casualUserChoice);
            if(casualUserChoice =="a"){
                cout<<"Adding Casual User. "<<endl;
                CasualUser temp = createCasualUser(Casuals);
                Casuals.push_back(temp);
                cout<<"Adding successful"<<endl;
            }            
            else if(casualUserChoice =="l"){
                cout<<"Logging in to Casual account. "<<endl;
                bool loggedIn = false;
                string tempName, tempPW;
                int casualUserIndex = -1;
                bool doesntExist = false;
                do{
                    cout<<"\nEnter your name: ";
                    getline(cin, tempName);
                    if(foundCasualUser(tempName, Casuals)){
                        doesntExist = false; 
                        casualUserIndex = findCasualUser(tempName, Casuals); 
                        for(int i = 0; i < 3; i++){
                            cout<< "Password: ";
                            getline(cin,tempPW);
                            if(tempPW == Casuals[casualUserIndex].getPassword()){
                                cout<<endl<<endl<<endl<<"Welcome, "<<Casuals[casualUserIndex].getName()<<endl;
                                loggedIn = true;
                                break;
                            }
                            else{cout<<"Wrong password. "<< 2 - i <<" attempts left"<<endl<<endl;}
                        
                            if(i == 3){cout<<"Goodbye."<<endl; exit(0);}
                        }
                        while(loggedIn){
                            cout<<"What would you like to do now, "<<Casuals[casualUserIndex].getName()<<"?"<<endl;
                            cout<<"View your transactions: 'v' \tAdd transaction: 'a' \tDelete transaction : 'd' \tLogout : 'l' \tQuit : 'q'"<<endl;
                            string loggedInOption;
                            getline(cin, loggedInOption);
                            if(loggedInOption == "l"){cout<<endl<<"Goodbye, "<<Casuals[casualUserIndex].getName()<<endl<<endl<<endl; loggedIn = false;}
                            else if(loggedInOption == "a"){Casuals[casualUserIndex].addTransaction(Casuals[casualUserIndex].recordTransaction());}
                            else if(loggedInOption == "d"){
                                Casuals[casualUserIndex].viewAllTransactions();
                                cout<<"Which would you like to delete? ('c' to Cancel)"<<endl; 
                                string casualDeleteChoice;
                                getline(cin, casualDeleteChoice);
                                if(casualDeleteChoice == "c"){ cout<<"Canceled, going back."<<endl;}
                                else{   
                                    int deleteIndex = atoi(casualDeleteChoice.c_str()) - 1;
                                    if(Casuals[casualUserIndex].TransactionListSize() == 0){cout<<"Nothing to delete"<<endl<<endl;}
                                    else if(deleteIndex < Casuals[casualUserIndex].TransactionListSize() + 1)
                                    {
                                        Casuals[casualUserIndex].deleteTransaction(deleteIndex);
                                    }
                                    else{cout<<"Invalid selection \n";}
                                    cout<<endl;
                                }
                            }
                            else if(loggedInOption == "q"){
                                cout<<"\nSaving and Quitting..... "<<endl; 
                                quit = true; 
                                loggedIn = false;
                            }
                            else if(loggedInOption == "v"){
                                cout<<"\n\n View all : 'v'\tView with 1 or more filtering factor : 'f'"<<endl;
                                string viewOption;
                                getline(cin,viewOption);
                                if(viewOption == "v"){
                                    Casuals[casualUserIndex].calcAndSetBalance(); 
                                    Casuals[casualUserIndex].viewAllTransactions();
                                }
                                else if(viewOption =="f"){
                                    cout<<"\nHow would you like to view your transactions?"<<endl;
                                    cout<<"Amount : 'a' \tType : 't' \tDate : 'd' \t "<<endl;
                                    string filterOption;
                                    getline(cin, filterOption);
                                    if(filterOption =="a"){
                                        cout<<"\nSet a  max amount : x \tSet a min amount : n \tSet a range: r"<<endl;
                                        string amountOption;
                                        getline(cin, amountOption);
                                        if(amountOption == "x"){
                                            double tempAmount;
                                            cout<<"\nEnter the maximum Transaction amount you want to be shown: $";
                                            string sTempAmount;
                                            getline(cin, sTempAmount);
                                            tempAmount = atof(sTempAmount.c_str());
                                            vector <Transaction> Matches;
                                            
                                            for (int i = 0; i < Casuals[casualUserIndex].TransactionListSize(); i++){
                                                
                                               
                                                    Transaction temp = Casuals[casualUserIndex].getTransaction(i);
                                                    if(temp.getAmount() <= tempAmount){
                                                        cout<<"Match"<<endl;
                                                        Matches.push_back(temp);
                                                    }
                                            }
                                            
                                            cout<<endl<<"Okay, here are the transactions that match:"<<endl;
                                            for(int i = 0; i < Matches.size(); i++){
                                                cout<<i + 1<< ". ";
                                                Matches[i].printMe();
                                            }
                                            
                                            
                                        }
                                        else if(amountOption == "n"){double tempAmount;
                                            cout<<"\nEnter the minimum Transaction amount you want to be shown: $";
                                            string sTempAmount;
                                           getline(cin, sTempAmount);
                                            tempAmount = atof(sTempAmount.c_str());
                                            vector <Transaction> Matches;
                                            
                                            for (int i = 0; i < Casuals[casualUserIndex].TransactionListSize(); i++){
                                                    Transaction temp = Casuals[casualUserIndex].getTransaction(i);
                                                    if(temp.getAmount() >= tempAmount){
                                                        Matches.push_back(temp);
                                                    }
                                            }
                                            
                                            cout<<endl<<"Okay, here are the transactions that match:"<<endl;
                                            for(int i = 0; i < Matches.size(); i++){
                                                cout<<i + 1<< ". ";
                                                Matches[i].printMe();
                                            }
                                        }
                                        else if(amountOption == "r"){
                                            vector <Transaction> Matches;
                                            double tempMin, tempMax;
                                            string sTempMin,  sTempMax;
                                            
                                            cout<<"\nEnter the minimum Transaction amount you want to be shown: $";
                                            getline(cin, sTempMin);
                                            tempMin = atof(sTempMin.c_str());
                                            
                                            cout<<"\n\nEnter The maximum transaction amount you want to be shown: $";
                                            getline(cin, sTempMax);
                                            tempMax = atof(sTempMax.c_str());
                                            
                                            if(tempMax < tempMin){
                                                double tempHolder = tempMax; 
                                                tempMax = tempMin; 
                                                tempMin = tempHolder;
                                            }
                                            
                                            for (int i = 0; i < Casuals[casualUserIndex].TransactionListSize(); i++){
                                                    Transaction temp = Casuals[casualUserIndex].getTransaction(i);
                                                    if(temp.getAmount() <= tempMax && temp.getAmount() >= tempMin ){
                                                        Matches.push_back(temp);
                                                    }
                                            }
                                            
                                            cout<<endl<<"Here are the transactions that match:"<<endl;
                                            for(int i = 0; i < Matches.size(); i++){
                                                cout<<i + 1<< ". ";
                                                Matches[i].printMe();
                                            }
                                            
                                        
                                        }
                                        else{cout<<"No correct response detected, try again."<<endl;}
                                        
                                    }
                                    else if(filterOption =="t"){
                                        cout<<"\nEnter the transaction type you want to be shown: ";
                                        string typeDesired;
                                        getline(cin, typeDesired);
                                        vector <Transaction> Matches;
                                        bool none = true;
                                        for (int i = 0; i < Casuals[casualUserIndex].TransactionListSize(); i++){
                                                Transaction temp = Casuals[casualUserIndex].getTransaction(i);
                                                
                                                if(temp.getType() == typeDesired){
                                                    Matches.push_back(temp);
                                                    none = false;
                                                }
                                        }
                                        if (none){cout<<"No matching transactions." <<endl;}
                                        else{
                                            cout<<endl<<"Okay, here are the transactions that match:"<<endl;
                                            for(int i = 0; i < Matches.size(); i++){
                                                cout<<i + 1<< ". ";
                                                Matches[i].printMe();
                                            }
                                        }

                                    
                                    
                                    }
                                    else if(filterOption =="d"){
                                        cout<<"\nSet an end date : 'e' \tSet a beginning date : b' \tSet a range: 'r'"<<endl;
                                        string amountOption;
                                        getline(cin, amountOption);
                                        if(amountOption == "e"){
                                            int endMonth, endDay, endYear;
                                            cout<<"\nSetting an end date."<<endl;
                                            do{
                                                cout<<"Month: ";
                                                string sEndMonth;
                                                getline(cin, sEndMonth);
                                                endMonth = atoi(sEndMonth.c_str());
                                                if(endMonth > 12 || endMonth < 1){cout<<"\nIncorrect Month.\n";}
                                            }while(endMonth < 0 || endMonth>12);
                                            
                                            do{
                                                cout<<"Day: ";
                                                string sEndDay;
                                                getline(cin, sEndDay);
                                                endDay = atoi(sEndDay.c_str());
                                                if(endDay > 31 || endDay < 1){cout<<"\nIncorrect Day.\n";}
                                            }while(endDay < 0 || endDay>31);
                                            
                                            do{
                                                cout<<"Year: ";
                                                string sEndYear;
                                                getline(cin, sEndYear);
                                                endYear = atoi(sEndYear.c_str());
                                                if(endYear > 99 || endYear < 0){cout<<"\nIncorrect Month.\n";}
                                            }while(endYear < 0 || endYear>99);
                                            
                                            cout<<"End date set: "<<endMonth<<"/"<<endDay<<"/"<<endYear<<endl;
                                            
                                            vector <Transaction> Matches;
                                            bool none = true;
                                            for (int i = 0; i < Casuals[casualUserIndex].TransactionListSize(); i++){
                                                    Transaction temp = Casuals[casualUserIndex].getTransaction(i);
                                                    if(firstDateisFirst(temp.getMonth(),temp.getDay(),temp.getYear(), endMonth, endDay, endYear)){
                                                        Matches.push_back(temp);
                                                        none = false;
                                                    }
                                            }
                                            if(none){cout<"No transactions found before this date\n\n";}
                                            else{
                                                cout<<endl<<"Okay, here are the transactions that match:"<<endl;
                                                for(int i = 0; i < Matches.size(); i++){
                                                    cout<<i + 1<< ". ";
                                                    Matches[i].printMe();
                                                }
                                            }
                                        }
                                        else if(amountOption == "b"){
                                            int startMonth, startDay, startYear;
                                            cout<<"\nSetting a start date."<<endl;
                                               do{
                                                   cout<<"Month: ";
                                                   string sStartMonth;
                                                   getline(cin, sStartMonth);
                                                   startMonth = atoi(sStartMonth.c_str());
                                                   if(startMonth > 12 || startMonth < 1){cout<<"\nIncorrect Month.\n";}
                                               }while(startMonth < 0 || startMonth>12);

                                               do{
                                                   cout<<"Day: ";
                                                   string sStartDay;
                                                   getline(cin, sStartDay);
                                                   startDay = atoi(sStartDay.c_str());
                                                   if(startDay > 31 || startDay < 1){cout<<"\nIncorrect Day.\n";}
                                               } while(startDay < 0 || startDay>31);

                                               do{
                                                   cout<<"Year: ";
                                                   string sStartYear;
                                                   getline(cin, sStartYear);
                                                   startYear = atoi(sStartYear.c_str());
                                                   if(startYear > 99 || startYear < 0){cout<<"\nIncorrect Month.\n";}
                                               }while(startYear < 0 || startYear>99);

                                               cout<<"Start date set: "<<startMonth<<"/"<<startDay<<"/"<<startYear<<endl;

                                               vector <Transaction> Matches;
                                               bool none = true;
                                               for (int i = 0; i < Casuals[casualUserIndex].TransactionListSize(); i++){
                                                       Transaction temp = Casuals[casualUserIndex].getTransaction(i);
                                                       if(firstDateisSecond(temp.getMonth(),temp.getDay(),temp.getYear(), startMonth, startDay, startYear)){
                                                           Matches.push_back(temp);
                                                           none = false;
                                                       }
                                               }
                                               if(none){
                                                   cout<"No transactions found before this date\n\n";
                                               }
                                               else{
                                                   cout<<endl<<"Okay, here are the transactions that match:"<<endl;
                                                   for(int i = 0; i < Matches.size(); i++){
                                                       cout<<i + 1<< ". ";
                                                       Matches[i].printMe();
                                                   }
                                               }

                                           }
                                        else if(amountOption == "r"){
                                            cout<<"Enter the start date."<<endl;
                                            string sFirMo,sFirDay, sFirYear, sSecMo, sSecDay, sSecYear;
                                            int firstMonth, firstDay, firstYear, secondMonth, secondDay, secondYear;
                                            
                                            do{
                                                cout<<"Month: ";
                                                string sFirMo;
                                                getline(cin, sFirMo);
                                                firstMonth = atoi(sFirMo.c_str());
                                                if(firstMonth > 12 || firstMonth < 1){cout<<"\nIncorrect Month.\n";}
                                            }while(firstMonth < 0 || firstMonth>12);

                                            do{
                                                cout<<"Day: ";
                                                string sFirDay;
                                                getline(cin, sFirDay);
                                                firstDay = atoi(sFirDay.c_str());
                                                if(firstDay > 31 || firstDay < 1){cout<<"\nIncorrect Day.\n";}
                                            } while(firstDay < 0 || firstDay>31);

                                            do{
                                                cout<<"Year: ";
                                                string sFirYear;
                                                getline(cin, sFirYear);
                                                firstYear = atoi(sFirYear.c_str());
                                                if(firstYear > 99 || firstYear < 0){cout<<"\nIncorrect Month.\n";}
                                            }while(firstYear < 0 || firstYear>99);

                                            cout<<"Start date set: "<<firstMonth<<"/"<<firstYear<<"/"<<firstYear<<endl;
                                            
                                            cout<<"\nSetting an end date."<<endl;
                                            do{
                                                cout<<"Month: ";
                                                string sSecMonth;
                                                getline(cin, sSecMonth);
                                                secondMonth = atoi(sSecMonth.c_str());
                                                if(secondMonth > 12 || secondMonth < 1){cout<<"\nIncorrect Month.\n";}
                                            }while(secondMonth < 0 || secondMonth>12);
                                            
                                            do{
                                                cout<<"Day: ";
                                                string sSecDay;
                                                getline(cin, sSecDay);
                                                secondDay = atoi(sSecDay.c_str());
                                                if(secondDay > 31 || secondDay < 1){cout<<"\nIncorrect Day.\n";}
                                            }while(secondDay < 0 || secondDay>31);
                                            
                                            do{
                                                cout<<"Year: ";
                                                string sSecYear;
                                                getline(cin, sSecYear);
                                                secondYear = atoi(sSecYear.c_str());
                                                if(secondYear > 99 || secondYear < 0){cout<<"\nIncorrect Month.\n";}
                                            }while(secondYear < 0 || secondYear>99);
                                            
                                            cout<<"End date set: "<<secondMonth<<"/"<<secondDay<<"/"<< secondYear<<endl;
                                            
                                            if(firstDateisFirst(secondMonth, secondDay, secondYear, firstMonth,firstDay,firstYear)){  
                                                int temporary;
                                                
                                                temporary =  secondMonth;
                                                secondMonth = firstMonth;
                                                firstMonth = temporary;
                                                
                                                temporary =  secondDay;
                                                secondDay = firstDay;
                                                firstDay = temporary;
                                                
                                                temporary =  secondYear;
                                                secondYear = firstYear;
                                                firstYear = temporary;
                                            }
                                            
                                            vector <Transaction> Matches;
                                            for(int i = 0; i < Casuals[casualUserIndex].TransactionListSize(); i++){
                                                Transaction temp = Casuals[casualUserIndex].getTransaction(i);
                                                
                                                if(firstDateisFirst(firstMonth,firstDay,firstYear,temp.getMonth(),temp.getDay(), temp.getYear())
                                                        &&         
                                                firstDateisFirst(temp.getMonth(),temp.getDay(), temp.getYear(), secondMonth, secondDay, secondYear)){
                                                    Matches.push_back(temp);
                                                    cout<<"Collected 1"<<endl;
                                                }
                                            
                                            }
                                            for(int i = 0; i < Matches.size(); i++){
                                                Matches[i].printMe();
                                            }
                                            
                                        }
                                        else {cout<<"\n\nNo correct response recieved\n\n";}
                                    }
                                    
                                    else{cout<<"\n \n No correct response detected, try again."<<endl<<endl<<endl;}
                                    
                                
                                
                                }    
                            }
                            else{cout<<"\nNo correct respnse detected\n\n";}
                            cout<<"\n";
                        }
                    }
                    else{cout<<endl<<endl<<"User not found. Goodbye."<<endl<<endl;} 
                }while(doesntExist);
            }
            else if(casualUserChoice =="q"){
                quit = true;
            }
            else{
                cout<<"Unknown selection. "<<endl;
            }            
        }
        else if(userTypeChoice == "p"){   
            //Professional Layer
            cout<<"\n\nProfessional User"<<endl;
            cout<<"What would you like to do? : "<<endl;
            cout<<"Quit: 'q' \t Add a new Boss: 'a' \t Login to a boss: 'b' \t Login to an Employee : 'e'"<<endl;
            string userInput;
            getline(cin,userInput);
            if(userInput == "a"){
                BossUser A = createBossUser(Bosses);
                Bosses.push_back(A);
            }
            else if(userInput == "b"){
                bool bossFound = false;  
                cout<<"\n\nLogging in to a BossUser account:"<<endl;
                cout<<"Enter name:";
                string searchName;
                getline(cin, searchName);
                bossFound = foundBossUser(searchName, Bosses);
                if(bossFound){
                    int bossIndex = findBossUser(searchName, Bosses);
                    cout<<"Enter password: ";
                    string tempPW;
                    getline(cin, tempPW); 
                    if(tempPW == Bosses[bossIndex].getPassword()){
                        cout<<endl<<endl<<"You are logged in, "<<Bosses[bossIndex].getName()<<"."<<endl;
                        bool loggedIn = true;
                        while(loggedIn){
                            
                            cout<<"\n\nWhat would you like to do now?"<<endl;
                            cout<<"View Your Employees' Expenses: 'x' \t Approve/Deny Transactions 'a'\t View/Add/Remove Employees 'e'\t Log out 'l'"<<endl<<endl;
                            string bossAction;
                            getline(cin, bossAction);
                            if(bossAction == "x") {
                                cout<< "Viewing Options: "<<endl;
                                cout<<"Viewing all expenses for each Employee:'x' \t View an employee's expenses by name 'n' \t"<<endl;
                                string viewFilter; 
                                getline(cin, viewFilter);
                                if(viewFilter ==  "x"){
                                    for(int i = 0; i < Bosses[bossIndex].RosterSize(); i++){
                                        Bosses[bossIndex].viewAllEmployeeExpenses(i);
                                    }
                                }                                
                                else if(viewFilter ==  "n"){
                                    cout<<"Enter the desired employee's name. "<<endl;
                                    string nameDesired;
                                    bool userExists = false;
                                    getline(cin, nameDesired);
                                    for(int i = 0; i < Bosses[bossIndex].RosterSize(); i++){
                                        CorporateEmployee tempUser = Bosses[bossIndex].getEmployee(i);
                                        if(tempUser.getName() == nameDesired){Bosses[bossIndex].viewAllEmployeeExpenses(i); userExists = true;}
                                    }
                                    if(!userExists){cout<<"\nNo employee's by that name.\n\n";}
                                
                                }
                                else{cout<<"No correct response recieved\n\n";}
                                cout<<"\n\n";
                            }
                            else if(bossAction == "e"){
                                cout<<"View employees: 'v' \t Add Emplyee : 'a' \t Remove Employee : 'r' \t Cancel : 'c'"<<endl;
                                string addRemoveQuit;
                                getline(cin, addRemoveQuit);
                                if(addRemoveQuit == "a"){
                                    cout<<"Adding employee:"<<endl;
                                    Bosses[bossIndex].addEmployee(Bosses[bossIndex].createEmployee());
                                    cout<<"Employee added."<<endl<<endl<<endl;
                                }
                                else if(addRemoveQuit == "v"){
                                    Bosses[bossIndex].viewAllEmployeeProfiles();
                                }
                                else if(addRemoveQuit == "r"){ 
                                    cout<<"Removing employee:"<<endl;
                                    Bosses[bossIndex].viewAllEmployeeProfiles();
                                    cout<<"Which employee would you like to remove from your Roster?"<<endl;
                                    string sRemoveIndex;
                                    getline(cin, sRemoveIndex);
                                    int removeIndex = atoi(sRemoveIndex.c_str()) - 1;
                                    
                                    if(Bosses[bossIndex].RosterSize() == 0){cout<<"No employees to delete"<<endl<<endl<<endl;}
                                    else if( removeIndex >= Bosses[bossIndex].RosterSize() ||removeIndex < 0 ){cout<< "Invalid deletion location."<<endl<<endl<<endl;}
                                    else{Bosses[bossIndex].deleteEmployee(removeIndex);}
                                }
                                else if(addRemoveQuit == "c"){cout<<"Canceled"<<endl<<endl<<endl;}
                                else{cout<<"No correct response recieved\n\n\n";}
                            }
                            else if(bossAction == "a"){
                                cout<<"\nApprove 'a' or deny 'd' expense requests?"<<endl;
                                string appOrDeny;
                                getline(cin, appOrDeny);
                                if(appOrDeny == "a"){
                                    cout<<"Approval options: "<<endl;
                                    cout<<"Approve all transactions of a certain type : 't'"<<endl;
                                    cout<<"Approve all transactions under a certain amount : 'm'"<<endl;
                                    cout<<"Approve all of a certain employee's transactions: 'e' "<<endl;
                                    cout<<"Approve a single expense from a certain Employee: 's' "<<endl;
                                    cout<<"Auto-approve all expenses : 'a'  (Not reccomended)"<<endl;
                                    string reviewMode;
                                    getline(cin, reviewMode);
                                    if(reviewMode == "a"){Bosses[bossIndex].approveAllUsersAllTransactions(true);}
                                    else if(reviewMode == "t"){
                                        string typeDesired;
                                        cout<<"Enter the type of expense you want to approve for all of your employees: "<<endl;
                                        cout<<"Type: ";
                                        getline(cin, typeDesired);
                                        Bosses[bossIndex].approveAllUsersByType(typeDesired, true);
                                    }
                                    else if(reviewMode == "m"){
                                        string sTempAmount;
                                        double tempAmount;
                                        cout<<"Approve all transactions below: $";
                                        getline(cin, sTempAmount);
                                        tempAmount = atof(sTempAmount.c_str());
                                        Bosses[bossIndex].approveAllUsersUnderAmount(tempAmount, true); 
                                    }
                                    else if(reviewMode == "e"){
                                        cout<<"\nWhich employee's expenses would you like to approve?"<<endl;
                                        string employeeToApprove;
                                        getline(cin, employeeToApprove);
                                        Bosses[bossIndex].approveAllOneUserByName(employeeToApprove, true);
                                    }
                                    else if(reviewMode == "s"){
                                        cout<<"\nWhich employee's expenses would you like to approve?"<<endl;
                                        string employeeToApprove;
                                        getline(cin, employeeToApprove);
                                        if(Bosses[bossIndex].employeeExists(employeeToApprove)){
                                            int employeeIndex = Bosses[bossIndex].findEmployeeIndex(employeeToApprove);
                                            CorporateEmployee tempEmployee = Bosses[bossIndex].getEmployee(employeeIndex);
                                            tempEmployee.viewAllExpenses();
                                            cout<<"Which transaction would you like to approve?  ('c' to cancel)"<<endl;
                                            string sApproveIndex;
                                            getline(cin, sApproveIndex);
                                            if(sApproveIndex == "c"){cout<<"Canceled."<<endl;}
                                            else{
                                                int approveIndex = atoi(sApproveIndex.c_str()) - 1;
                                                if(approveIndex < 0 || approveIndex >= tempEmployee.expenseListSize()){
                                                    cout<<"Invalid approval index."<<endl;                                            
                                                }
                                                else{
                                                    tempEmployee.BossSetApproved(approveIndex, true);
                                                    cout<<"Successfully approved transactions."<<endl;
                                                }
                                            }
                                            Bosses[bossIndex].replaceEmployee(tempEmployee, employeeIndex);
                                        }
                                        else{cout<<"\nYou dont have an employee by that name."<<endl;}
                                        cout<<"\n\n";
                                    }
                                    else{cout<<"\nNo correct response recieved."<<endl<<endl<<endl;}
                                } 
                                else if(appOrDeny == "d"){
                                    
                                    cout<<"Request Denial options:"<<endl;
                                    cout<<"Deny all transactions of a certain type : 't'"<<endl;
                                    cout<<"Deny all transactions under a certain amount : 'm'"<<endl;
                                    cout<<"Deny all of a certain employee's transactions: 'e' "<<endl;
                                    cout<<"Deny a single expense from a certain Employee: 's' "<<endl;
                                    cout<<"Auto-deny all expenses (Not reccomended): 'a'  "<<endl;
                                    string denialOption;
                                    getline(cin, denialOption);
                                    if(denialOption =="t"){
                                        string typeDesired;
                                        cout<<"Enter the type of expense you want to approve for all of your employees: "<<endl;
                                        cout<<"Type: ";
                                        getline(cin, typeDesired);
                                        Bosses[bossIndex].approveAllUsersByType(typeDesired, false);
                                    }
                                    else if(denialOption =="m"){
                                        string sTempAmount;
                                        double tempAmount;
                                        cout<<"Approve all transactions below: $";
                                        getline(cin, sTempAmount);
                                        tempAmount = atof(sTempAmount.c_str());
                                        Bosses[bossIndex].approveAllUsersUnderAmount(tempAmount, false); 
                                    }
                                    else if(denialOption =="e"){
                                        cout<<"\nWhich employee's expenses would you like to approve?"<<endl;
                                        string employeeToApprove;
                                        getline(cin, employeeToApprove);
                                        Bosses[bossIndex].approveAllOneUserByName(employeeToApprove, true);
                                    
                                    }
                                    else if(denialOption =="s"){
                                        cout<<"\nWhich employee's expenses would you like to deny?"<<endl;
                                        string employeeToDeny;
                                        getline(cin, employeeToDeny);
                                        if(Bosses[bossIndex].employeeExists(employeeToDeny)){
                                            int employeeIndex = Bosses[bossIndex].findEmployeeIndex(employeeToDeny);
                                            CorporateEmployee tempEmployee = Bosses[bossIndex].getEmployee(employeeIndex);
                                            tempEmployee.viewAllExpenses();
                                            cout<<"Which transaction would you like to deny?  ('c' to cancel)"<<endl;
                                            string sApproveIndex;
                                            getline(cin, sApproveIndex);
                                            if(sApproveIndex == "c"){cout<<"Canceled."<<endl;}
                                            else{
                                                int approveIndex = atoi(sApproveIndex.c_str()) - 1;
                                                if(approveIndex < 0 || approveIndex >= tempEmployee.expenseListSize()){
                                                    cout<<"Invalid denial index."<<endl;                                            
                                                }
                                                else{
                                                    tempEmployee.BossSetApproved(approveIndex, false);
                                                    cout<<"Successfully denied transactions."<<endl;
                                                }
                                            }
                                            Bosses[bossIndex].replaceEmployee(tempEmployee, employeeIndex);
                                        }
                                        else{cout<<"\nYou dont have an employee by that name."<<endl;}
                                        cout<<"\n\n";
                                    }
                                    else if(denialOption =="a"){Bosses[bossIndex].approveAllUsersAllTransactions(false);}
                                
                                
                                }
                                else{cout<<"\nNo correct response recieved. "<<endl;}
                                cout<<"\n\n";
                            }
                            else if(bossAction == "l"){loggedIn = false;}
                            else{cout<<"Unknown response, "<<Bosses[bossIndex].getName()<<". "<<endl;}
                        }
                    }
                }
                else{cout<<"Sorry, that boss not found"<<endl;}
            }
            else if(userInput == "e"){
                
                cout<<"Logging in to an Employee account."<<endl;
                cout<<"Enter your employer's name: ";
                string employeeBossesName;
                bool bossExists = false;
                bool loggedIn = false;
                int employerIndex, employeeRosterIndex;
                
                getline(cin, employeeBossesName);
                
                for(int i = 0; i <  Bosses.size(); i++){
                    if(employeeBossesName == Bosses[i].getName()){
                        bossExists = true;
                        employerIndex = i;
                        break;
                    }
                }
                if(bossExists){
                    cout<<"Enter your name:";
                    string employeeUserName;
                    getline(cin, employeeUserName);
                    if (Bosses[employerIndex].employeeExists(employeeUserName)){
                        employeeRosterIndex = Bosses[employerIndex].findEmployeeIndex(employeeUserName);
                        //pulls from a BossUsers private vector into a temp issue to make scope easier to manage;
                        CorporateEmployee tempEmployee = Bosses[employerIndex].getEmployee(employeeRosterIndex);
                        for(int i = 0; i < 3; i++){
                            cout<<"Enter your password: ";
                            string tempPW;
                            getline(cin, tempPW);
                            cout<<"\n\n";
                            if(tempPW == tempEmployee.getPassword()){loggedIn = true; break;}
                            else{
                                cout<<"Password Incorrect. "<<2-i<<" attempts remaining."<<endl;
                                if (i == 2){quit = true;}
                            }
                        }
                        cout<<"Greetings, "<<tempEmployee.getName()<<". "<<endl;
                        while(loggedIn){
                            cout<<"What would you like to do ? \t Quit: 'q' \t Log out 'l'"<<endl;
                            cout<<"View transactions : 'v' \t Add Transactions 'a' \t Delete Transactions 'd' \t Edit Transaction 'e'\n\n\n";
                            
                            string userChoice;
                            getline(cin, userChoice);
                            
                            if(userChoice=="q"){quit = true; loggedIn = false;}
                            else if(userChoice=="l"){loggedIn = false;}
                            else if(userChoice=="v"){
                                cout<<"Viewing Transactions: "<<endl;
                                cout<<"View all: 'v' \t View through filter 'f'"<<endl;
                                string filterOrNot;
                                getline(cin, filterOrNot);
                                cout<<"\n\n";
                                
                                if(filterOrNot == "v"){tempEmployee.viewAllExpenses();}
                                else if(filterOrNot == "f"){
                                    cout<<"Filter by amount: 'm' \t Filter by type: 't' \t Filter by Approval 'p' \t Filter by date: 'd'"<<endl;
                                    string filterOption;
                                    getline(cin, filterOption);
                                    if(filterOption == "m"){
                                        cout<<"Set max: 'x' \t Set min 'n' \t setRange: 'r'";
                                        string amountOption;
                                        getline(cin, amountOption);
                                        if(amountOption == "x"){
                                            cout<<"Enter the maximum amount you want to be shown: $";
                                            string sMaxAmount;
                                            double maxAmount;
                                            getline(cin, sMaxAmount);
                                            maxAmount = atof(sMaxAmount.c_str());
                                            tempEmployee.viewUnderMax(maxAmount);   
                                        }
                                        else if(amountOption == "n"){
                                            cout<<"Enter the minimum amount you want to be shown: $";
                                            string sMinAmount;
                                            double minAmount;
                                            getline(cin,sMinAmount);
                                            minAmount = atof(sMinAmount.c_str());
                                            tempEmployee.viewOverMin(minAmount);
                                        }
                                        else if(amountOption == "r"){
                                            cout<<"Enter the minimum amount you want to be shown: $";
                                            string sMinAmount;
                                            double minAmount;
                                            getline(cin,sMinAmount);
                                            minAmount = atof(sMinAmount.c_str());
                                            cout<<"Enter the maximum amount you want to be shown: $";
                                            string sMaxAmount;
                                            double maxAmount;
                                            getline(cin, sMaxAmount);
                                            maxAmount = atof(sMaxAmount.c_str());
                                            tempEmployee.viewInRange(minAmount, maxAmount);
                                        
                                        }
                                        else{cout<<"No correct response recieved.";}
                                        cout<<"\n\n\n";
                                    }
                                    else if(filterOption == "t"){
                                        cout<<"Enter the type of transaction you want to be shown: ";
                                        string typeDesired;
                                        getline(cin, typeDesired);
                                        tempEmployee.viewByType(typeDesired);
                                    }
                                    else if(filterOption == "p"){
                                        cout<<"Filtering by approval:"<<endl;
                                        cout<<"View transactions that have been: \t Approved 'a' \t Denied 'd' \t Not yet reviewed 'r' "<<endl;
                                        string approvalOption;
                                        getline(cin, approvalOption);
                                        if(approvalOption == "a"){
                                            tempEmployee.viewApproved();
                                        }
                                        else if(approvalOption == "d"){
                                            tempEmployee.viewDenied();
                                        }
                                        else if(approvalOption == "r"){
                                            tempEmployee.viewPending();
                                        }
                                        else{cout<<"No correct response recieved"<<endl;}
                                        cout<<endl<<endl;
                                    
                                    }
                                    else if(filterOption == "d"){
                                        cout<<"Viewing your expenses by date: \t Set a start date 's' \t Set an end date 'e' \t Set a range 'r':  ";
                                        string dateOption;
                                        getline(cin, dateOption);
                                        if(dateOption =="s"){
                                            string sTempMo, sTempDay, sTempYear;
                                            int tempMo, tempDay, tempYear;
                                            cout<<"Enter your start date."<<endl;
                                            cout<<"Month: ";
                                            getline(cin, sTempMo);
                                            tempMo = atoi(sTempMo.c_str());
                                            cout<<"Day: ";
                                            getline(cin, sTempDay);
                                            tempDay = atoi(sTempDay.c_str());
                                            cout<<"Year: ";
                                            getline(cin, sTempYear);
                                            tempYear = atoi(sTempYear.c_str());
                                            cout<<endl;
                                            tempEmployee.viewAfterDate(tempMo, tempDay, tempYear);
                                        }
                                        else if(dateOption =="e"){
                                            string sTempMo, sTempDay, sTempYear;
                                            int tempMo, tempDay, tempYear;
                                            cout<<"Enter your end date."<<endl;
                                            cout<<"Month: ";
                                            getline(cin, sTempMo);
                                            tempMo = atoi(sTempMo.c_str());
                                            cout<<"Day: ";
                                            getline(cin, sTempDay);
                                            tempDay = atoi(sTempDay.c_str());
                                            cout<<"Year: ";
                                            getline(cin, sTempYear);
                                            tempYear = atoi(sTempYear.c_str());
                                            cout<<endl;
                                            tempEmployee.viewBeforeDate(tempMo, tempDay, tempYear);
                                        }
                                        else if(dateOption =="r"){
                                            string sTempBegMo, sTempBegDay, sTempBegYear;
                                            int tempBegMo, tempBegDay, tempBegYear;
                                            cout<<"Enter your start date."<<endl;
                                            cout<<"Month: ";
                                            getline(cin, sTempBegMo);
                                            tempBegMo = atoi(sTempBegMo.c_str());
                                            cout<<"Day: ";
                                            getline(cin, sTempBegDay);
                                            tempBegDay = atoi(sTempBegDay.c_str());
                                            cout<<"Year: ";
                                            getline(cin, sTempBegYear);
                                            tempBegYear = atoi(sTempBegYear.c_str());
                                            cout<<endl;
                                            
                                            string sTempEndMo, sTempEndDay, sTempEndYear;
                                            int tempEndMo, tempEndDay, tempEndYear;
                                            cout<<"\nEnter your end date."<<endl;
                                            cout<<"Month: ";
                                            getline(cin, sTempEndMo);
                                            tempEndMo = atoi(sTempEndMo.c_str());
                                            cout<<"Day: ";
                                            getline(cin, sTempEndDay);
                                            tempEndDay = atoi(sTempEndDay.c_str());
                                            cout<<"Year: ";
                                            getline(cin, sTempEndYear);
                                            tempEndYear = atoi(sTempEndYear.c_str());
                                            cout<<endl;
                                            tempEmployee.viewBetweenDates(tempBegMo, tempBegDay, tempBegYear, tempEndMo, tempEndDay, tempEndYear);
                                        }
                                        else{cout<<"No correct response recieved."<<endl;}
                                        cout<<"\n\n";
                                    
                                    }
                                    else{cout<<"No correct response recieved\n";}
                                    cout<<"\n\n";
                                    
                                }
                                else{cout << "No correct response recieved. \n";}
                                cout<<"\n\n";
                            
                            }
                            else if(userChoice=="a"){tempEmployee.addExpense(tempEmployee.recordExpense());}
                            else if(userChoice=="d"){
                                cout<<"Deleting an expense: "<<endl;
                                tempEmployee.viewAllExpenses();
                                cout<<"Which expense would you like to delete? "<<endl;
                                string sDeleteIndex;
                                int deleteIndex;
                                getline(cin, sDeleteIndex);
                                deleteIndex = atoi(sDeleteIndex.c_str()) - 1;
                                if(tempEmployee.expenseListSize() == 0){cout <<"Nothing to delete. "<<endl;}
                                else if(deleteIndex < 0 || deleteIndex >=  tempEmployee.expenseListSize()){cout<<"Invalid deletion location.\n";}
                                else{tempEmployee.deleteExpense(deleteIndex);}
                                cout<<endl<<endl;
                            }
                            else if(userChoice=="e"){
                                cout<<"Editing Expense:"<<endl;
                                tempEmployee.viewAllExpenses();
                                cout<<"Which expense would you like to edit? "<<endl;
                                string sEditIndex;
                                int editIndex;
                                getline(cin, sEditIndex);
                                editIndex = atoi(sEditIndex.c_str()) - 1;
                                if(tempEmployee.expenseListSize() == 0){cout<<"Nothing to delete"<<endl;}
                                else if(editIndex < 0 || editIndex >= tempEmployee.expenseListSize()){cout<<"Invalid edit index. "<<endl;}
                                else{tempEmployee.editExpense(editIndex);}
                            }
                            else{cout<<"No correct response recieved"<<endl;}
                            cout<<"\n\n";
                        }
                        //Saves temp back into BossUser's private vector
                        Bosses[employerIndex].replaceEmployee(tempEmployee, employeeRosterIndex);   
                    }
                    else{cout<<"Sorry, that employee was not found."<<endl;}
                    cout<<"\n\n";
                }
                else{cout<<"Sorry, that employer was not found."<<endl;}
                cout<<"\n\n";
                
            }
            else if(userInput == "q"){quit = true;}
            else{cout<<"Unknown response."<<endl;}
            cout<<"\n\n\n";
        }
        else{cout<<"Sorry, no correct response detected"<<endl;}
        cout<<"\n\n\n";
    }
    //Save Bosses, Employees via Bosses, and Casuals
    saveBossUserList(Bosses);
    for(int i = 0; i<Bosses.size(); i++){
        Bosses[i].saveRoster();
        Bosses[i].saveAllEmployeeExpenses();
    }
    saveCasualUserList(Casuals);
    for(int i = 0; i<Casuals.size(); i++){
        Casuals[i].saveTransactions();
    }
    cout<<"\nGoodbye!"<<endl;
    return 0;
}
